<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
			'identityCookie' => ['name' => '_bidentity', 'httpOnly' => true],
            'enableAutoLogin' => true,
        ],
		'session' => [
            'class' => 'yii\web\DbSession',
        ],
		'i18n' => [
            'class' => common\components\I18N::className(),
			'translations' => [
				'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'messageTable' => 'message',
                    'sourceMessageTable' => 'source_message',
				],
			],
            'languages' => ['en', 'de']
		],		        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],        
    ],
    'params' => $params,
];
