<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "administrators".
 *
 * @property integer $AdministratorID
 * @property string $Nickname
 * @property string $Password
 * @property integer $Enabled
 * @property string $Name
 * @property string $Email
 * @property string $Note
 */
class Administrators extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nickname', 'Password', 'Enabled', 'Name', 'Email', 'Note'], 'required'],
            [['Enabled'], 'integer'],
            [['Note'], 'string'],
            [['Nickname', 'Password'], 'string', 'max' => 20],
            [['Name', 'Email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AdministratorID' => Yii::t('app', 'Administrator ID'),
            'Nickname' => Yii::t('app', 'Nickname'),
            'Password' => Yii::t('app', 'Password'),
            'Enabled' => Yii::t('app', 'Enabled'),
            'Name' => Yii::t('app', 'Name'),
            'Email' => Yii::t('app', 'Email'),
            'Note' => Yii::t('app', 'Note'),
        ];
    }
}
