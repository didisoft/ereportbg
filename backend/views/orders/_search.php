<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'OrderID') ?>

    <?= $form->field($model, 'OrderDate') ?>

    <?= $form->field($model, 'CustomerID') ?>

    <?= $form->field($model, 'PlanID') ?>

    <?= $form->field($model, 'Period') ?>

    <?php // echo $form->field($model, 'Amount') ?>

    <?php // echo $form->field($model, 'MOL') ?>

    <?php // echo $form->field($model, 'EIK') ?>

    <?php // echo $form->field($model, 'Address') ?>

    <?php // echo $form->field($model, 'CompanyName') ?>

    <?php // echo $form->field($model, 'PaymentMethodID') ?>

    <?php // echo $form->field($model, 'OrderStatusID') ?>

    <?php // echo $form->field($model, 'IN_DDS') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
