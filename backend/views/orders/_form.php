<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'OrderDate')->textInput() ?>

    <?= $form->field($model, 'CustomerID')->textInput() ?>

    <?= $form->field($model, 'PlanID')->textInput() ?>

    <?= $form->field($model, 'Period')->textInput() ?>

    <?= $form->field($model, 'Amount')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'MOL')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'EIK')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'Address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'CompanyName')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'PaymentMethodID')->textInput() ?>

    <?= $form->field($model, 'OrderStatusID')->textInput() ?>

    <?= $form->field($model, 'IN_DDS')->textInput(['maxlength' => 20]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
