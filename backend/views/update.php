<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hints */

$this->title = 'Update Hints: ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Hints', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->HintID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hints-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
