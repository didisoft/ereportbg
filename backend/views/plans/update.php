<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Plans */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Plans',
]) . ' ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->PlanID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="plans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
