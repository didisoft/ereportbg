<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hints */

$this->title = 'Create Hints';
$this->params['breadcrumbs'][] = ['label' => 'Hints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hints-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
