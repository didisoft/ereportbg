<?php
/**
 * @var View $this
 * @var SourceMessage $model
 */

use yii\helpers\Html;
use yii\web\View;
use common\models\translate\SourceMessage;
use \yii\widgets\ActiveForm;

$this->title = 'Update' . ': ' . $model->message;
$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Translate';
?>
<div class="message-update">
    <div class="message-form">
        <?= 'Source message' ?>
        <b><?= Html::encode($model->message) ?></b>
        <?php $form = ActiveForm::begin(); ?>
        <div class="field">
            <div class="ui grid">
                <?php foreach ($model->messages as $language => $message) : ?>
                    <div class="four wide column">
                        <?= $form->field($model->messages[$language], '[' . $language . ']translation')->label($language)->textarea() ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?= Html::submitButton('Update', ['class' => 'ui primary button']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['translate/index']) ?>
        <?php $form::end(); ?>
    </div>
</div>