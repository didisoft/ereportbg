<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use common\models\Plans;
use common\models\Logins;

use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CompanyName')->textInput(['maxlength' => 255]) ?>

<?php 
//use app\models\Country;
$countries = Plans::find()->all();

//use yii\helpers\ArrayHelper;
$listData = ArrayHelper::map($countries,'PlanID','Name');

echo $form->field($model, 'PlanID')->dropDownList(
								$listData, 
								['prompt'=>'Select...']
								);
?>	

    <?= $form->field($model, 'RegistrationTime')->textInput() ?>

    <p>Plan Expiration Time</p>
    
	<?php echo DatePicker::widget([
		'model' => $model, 
		'attribute' => 'PlanExpirationTime',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'yyyy-mm-dd'
		],
		'options' => [],
	]);
	?>
	
    <?= $form->field($model, 'Discount')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'activkey')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Active')->checkbox() ?>
    <?= $form->field($model, 'PlanActive')->checkbox() ?>

    <?= $form->field($model, 'Test')->checkbox() ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    <?php 
    echo GridView::widget([
    'dataProvider' => new yii\data\ActiveDataProvider(['query' => Logins::find()->andFilterWhere(['CustomerID' => $model->CustomerID])]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'LoginID',
        'FirstName',
        'LastName',
        'Email:email',
        'created_at',
        ]
        ]);
    ?>
</div>
