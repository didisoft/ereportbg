<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Hints */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Hints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hints-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->HintID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->HintID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'HintID',
            'Name',
            'Value:ntext',
        ],
    ]) ?>

</div>
