<?php
namespace backend\controllers;

use Yii;
use backend\models\GoogleSearch;

class GoogleController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$model = new GoogleSearch();
		if (Yii::$app->request->isPost) {			
			if ($model->load(Yii::$app->request->post())) {
				$client = new \Google_Client();//Client();
				$client->setApplicationName(\Yii::$app->params['google_app_name']);
				$client->setDeveloperKey(\Yii::$app->params['google_api_key']);
				$service = new \Google_Service_Customsearch($client);

				$cse = $service->cse; 
				
				// https://developers.google.com/custom-search/docs/xml_results#num

				$word = urlencode( $model->q );				
				// engine id
				$params = array('cx'=> \Yii::$app->params['google_cse_cx_id']);
				// days
				$params = empty($model->days) ? $params : array_merge($params, ['dateRestrict'=>'d'. $model->days]);
				
				$list = $cse->listCse($word, 
									  $params);
				
				//echo var_dump($list);
				
				// Google_Service_Customsearch_SearchSearchInformation
				$info = $list->getSearchInformation();
				$totalResults = $info->getTotalResults();
				$resultsPerPage = 10;
				$pagesCount = $totalResults / $resultsPerPage;
					
				// array Google_Service_Customsearch_Result
				$results = $list->getItems();
				
				// list additional pages
				$additionalPages = 2;
				if ($pagesCount > $additionalPages) 
				 for ($i = 1; $i < $additionalPages; $i++) {
					$params = array_merge($params, array('start' => $i * $resultsPerPage + 1));
					$list = $cse->listCse($word, 
										  $params);
										  
					$results = array_merge( $results, $list->getItems() );
				 }

				echo var_dump( count($results) );
				//$newResut->title = $result->getTitle();
				//$newResut->link = $result->getLink();
				//$newResut->snippet = $result->getSnippet();				
				
				
				exit;
			}
		}
	
		$results = [];
        return $this->render('index', 
							['results' => $results,
							 'model' => $model]);
    }

}
