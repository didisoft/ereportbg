<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = $title;	
?>
<p>
    <?= $content ?>
</p>
<p>
    <?= $links ?>
</p>
<p>
    <?= $antispam ?>
</p>