<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

if (\Yii::$app->params['language'] === 'bg-BG') { 
	$this->title = \Yii::$app->params['siteName'] . ' Получено плащане';	
} else {
	$this->title = \Yii::$app->params['siteName'] . ' Zahlungsbest�tigung';
}	


$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['billing/invoice-external', 'id' => $id, 'key' => $key]);
?>
<div class="password-reset">
    <p>Hi <?= $userName ?>,</p>

    <p>Vielen Dank f�r Ihre Zahlung f�r<?= \Yii::$app->params['siteName'] ?>
	<br/>
	Ihr Abonnementplan"<?= $plan ?>" wurde bis <?= $expiration ?> aktiviert.
	</p>	
<br/>
<br/>
   Eventuell benötigen Sie eine Weile, um sich an <?= \Yii::$app->params['siteName'] ?> zu gewöhnen und wie es gehandhabt wird. 
   Unser Supportteam ist bereit Sie jederzeit zu unterstützen, kontaktieren Sie uns ganz einfach über 
   <a href="https://www.erinnerungsbot.de/hilfe/">https://www.erinnerungsbot.de/hilfe/</a>. 
