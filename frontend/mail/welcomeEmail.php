<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '['.\Yii::$app->params['siteName'] . '] ' . 'Aktivierung';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hallo <?= $user->FirstName ?>,</p>

    <p>Willkommen zu <?= \Yii::$app->params['siteName'] ?></p>

	<p>Ihre Anmeldung ist nun aktiv. Melden Sie sich in unserem Dashboard an und starten Sie Ihren Terminplan:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
   Eventuell benötigen Sie eine Weile, um sich an <?= \Yii::$app->params['siteName'] ?> zu gewöhnen und wie es gehandhabt wird. 
   Unser Supportteam ist bereit Sie jederzeit zu unterstützen, kontaktieren Sie uns ganz einfach über 
   <a href="https://www.erinnerungsbot.de/hilfe/">https://www.erinnerungsbot.de/hilfe/</a>. 
</div>