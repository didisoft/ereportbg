<?php 
	$this->title = \Yii::$app->params['siteName'] . ' Subscription about to expire';
?>
<p>Hi <?= $userName ?>,</p>

Your subscription for the services of <?= \Yii::$app->params['siteName'] ?> is about to expire in 7 days.
<br/>
<br/>
You can renew it from menu Subscription at <?= Html::a(Html::encode(\Yii::$app->params['siteName']), Yii::$app->urlManager->createAbsoluteUrl(['/'])) ?>
<br/>
<br/>
<?= $emailSignature ?>
