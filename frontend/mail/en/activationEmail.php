<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '['.\Yii::$app->params['siteName'] . '] ' . 'Activation';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate', 'id' =>$user->LoginID, 'token' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Hi <?= $userName ?>,</p>

    <p>Thank you for choosing <?= \Yii::$app->params['siteName'] ?></p>
    
	<p>Please visit the web address below in order to activate your registration:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
    <p>You will then be able to complete sign up and start using your appointment reminders schedule.    </p>
    
    <p>Your Control Center login details are:</p>

    <p>Username:  	<?= $email ?></p>
    <p>Password:  	<?= $password ?></p>
    <p>Account Number:  	<?= $customerId ?></p>

    <p>Please save this email with your important account information.</p>

    <?= $emailSignature ?>
</div>