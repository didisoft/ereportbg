<?php
use yii\helpers\Html;

$this->title = \Yii::$app->params['siteName'] . ' Subscription Expired';
?>
<p>Hi <?= $userName ?>,</p>

Your subscription for the services of <?= Html::a(Html::encode(\Yii::$app->params['siteName']), Yii::$app->urlManager->createAbsoluteUrl(['/'])) ?> has expired.
<br/>
<br/>
You can renew it from menu Subscription at <?= Html::a(Html::encode(\Yii::$app->params['siteName']), Yii::$app->urlManager->createAbsoluteUrl(['/'])) ?>
<br/>
<br/>
<?= $emailSignature ?>
