<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '['.\Yii::$app->params['siteName'] . '] ' . 'Activation';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>Welcome to <?= \Yii::$app->params['siteName'] ?></p>
    
    <p>Below are the details of your account:</p>

	<p>Dashboard login at:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>

    <p>User: <?= $user->Email ?></p>

    <p>Password: <?= $password ?></p>

    It may take a little time to get used to <?= \Yii::$app->params['siteName'] ?> Dashboard and how the system works. 
    Our support team is ready to assist you in any way we can, so please contact us https://support.fastspring.com. 

</div>