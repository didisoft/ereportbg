<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '['.\Yii::$app->params['siteName'] . '] ' . 'Activation';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Dear <?= $userName ?>,</p>

    <p>Welcome to <?= \Yii::$app->params['siteName'] ?></p>

	<p>Your registration is now active. Sign into our Dashboard and start using your appointment reminders schedule:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
    <?= $emailSignature ?>
</div>