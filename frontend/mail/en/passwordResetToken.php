<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = \Yii::$app->params['siteName'] . \Yii::t('app', 'Lost password');	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hi <?= $userName ?>,</p>

    <p>Please follow the link below in order to reset your password for <?= \Yii::$app->params['siteName'] ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
<?= $emailSignature ?>
