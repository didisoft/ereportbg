<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

if (\Yii::$app->params['language'] === 'bg-BG') { 
	$this->title = \Yii::$app->params['siteName'] . ' Получено плащане';	
} else {
	$this->title = \Yii::$app->params['siteName'] . ' Payment Confirmation';
}	


$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['billing/invoice-external', 'id' => $id, 'key' => $key]);
?>
<div class="password-reset">
    <p>Hi <?= $userName ?>,</p>

    <p>Thank your for the payment for the services of <?= \Yii::$app->params['siteName'] ?>
	<br/>
	Your subscription plan "<?= $plan ?>" has been activated till <?= $expiration ?>.
	</p>	
<br/>
<br/>
<?= $emailSignature ?>