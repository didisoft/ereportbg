<?php
use yii\helpers\Html;

$this->title = \Yii::$app->params['siteName'] . ' Subscription Expired';
?>
<p>Hallo <?= $user->FirstName ?>,</p>

Ihr Abonnement für <?= Html::a(Html::encode(\Yii::$app->params['siteName']), Yii::$app->urlManager->createAbsoluteUrl(['/'])) ?> ist abgelaufen.
<br/>
<br/>
Sie können es im Menü Abonnement hier erneuern <?= Html::a(Html::encode(\Yii::$app->params['siteName']), Yii::$app->urlManager->createAbsoluteUrl(['/'])) ?>
<br/>
<br/>
   Eventuell benötigen Sie eine Weile, um sich an <?= \Yii::$app->params['siteName'] ?> zu gewöhnen und wie es gehandhabt wird. 
   Unser Supportteam ist bereit Sie jederzeit zu unterstützen, kontaktieren Sie uns ganz einfach über 
   <a href="https://www.erinnerungsbot.de/hilfe/">https://www.erinnerungsbot.de/hilfe/</a>. 

