<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = \Yii::$app->params['siteName'] . \Yii::t('app', 'Passwort vergessen');	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hallo <?= $user->FirstName ?>,</p>

    <p>Bitte folgen Sie dem Link unten, um Ihr Passwort für <?= \Yii::$app->params['siteName'] ?> zurückzusetzen.</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
   Eventuell benötigen Sie eine Weile, um sich an <?= \Yii::$app->params['siteName'] ?> zu gewöhnen und wie es gehandhabt wird. 
   Unser Supportteam ist bereit Sie jederzeit zu unterstützen, kontaktieren Sie uns ganz einfach über 
   <a href="https://www.erinnerungsbot.de/hilfe/">https://www.erinnerungsbot.de/hilfe/</a>. 

