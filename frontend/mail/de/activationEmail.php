<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '['.\Yii::$app->params['siteName'] . '] ' . 'Registrierung';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate', 'id' =>$user->LoginID, 'token' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Vielen Dank, dass Sie sich für <?= \Yii::$app->params['siteName'] ?> entschieden haben</p>
    
	<p>Bitte besuchen Sie die Internetseite unten um Ihre Registrierung zu vervollständigen:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
    <p>Danach werden Sie die Möglichkeit haben sich einzuloggen und Ihre Terminerinnerungspläne zu gestalten.</p>
    
    <p>Ihre Kontrollzentrum Login-Angaben sind:</p>

    <p>Benutzer:  	<?= $user->Email ?></p>
    <p>Passwort:  	<?= $password ?></p>

    <p>Bitte bewahren Sie diese E-Mail auf, da sie wichtige Zugangsinformationen enthält.</p>

   Eventuell benötigen Sie eine Weile, um sich an <?= \Yii::$app->params['siteName'] ?> zu gewöhnen und wie es gehandhabt wird. 
   Unser Supportteam ist bereit Sie jederzeit zu unterstützen, kontaktieren Sie uns ganz einfach über 
   <a href="https://www.erinnerungsbot.de/hilfe/">https://www.erinnerungsbot.de/hilfe/</a>. 
</div>