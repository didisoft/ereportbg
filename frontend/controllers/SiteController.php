<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\Customers;
use common\models\User;
use common\models\Plans;
use common\controllers\MyController;
use common\models\AuthConnect;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends MyController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
			'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'socialLoginCallback'],
            ],			
			'reg' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'socialRegisterCallback'],
            ],			
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    
    
    // https://github.com/yiisoft/yii2-authclient/blob/master/docs/guide/quick-start.md
    public function socialLoginCallback($client) {
        
        $email = $this->getAttrEmail($client);
        $user = User::findOne(['Email'=>$email]);
        if (!empty($user)) {
            Yii::$app->user->login($user);
        } else {
            $found = AuthConnect::findOne(['Email' => $email]);
            if ($found) {
                $user = \common\models\User::findOne(['LoginID'=>$found->LoginID]);
                Yii::$app->user->login($user);                
            } 
            else
            {
                Yii::$app->getSession()->setFlash('error', [
                                    Yii::t('app',
                                        'Unable to find account for {email}. Would you like to Register?',
                                        ['email' => $email]),
                                ]);                    
            }
        }        
    }



    public function socialRegisterCallback($client) {
        $attributes = $client->getUserAttributes();
        $nickname = '';
        if ($client->getName() == 'google') {
            $nickname = $attributes['displayName'];
        } else if ($client->getName() == 'facebook') {
            $nickname = $attributes['first_name'];
        }
        $email = $this->getAttrEmail($client);
        if (User::findOne(['Email'=>$email])) {
            $this->socialLoginCallback($client);
            return;
        } else if (AuthConnect::findOne(['Email' => $email])) {
            $this->socialLoginCallback($client);
            return;
        }
        
		$password = $this->randomPassword();
				
        $signup = new SignupForm();
        $signup->name = $nickname;
        $signup->email = $email;
        $signup->companyName = '';
        $signup->password = $password;
        $signup->countryCode = 'DE';
        $user = $signup->signup();
        
        $customer = Customers::findOne(['CustomerID' => $user->CustomerID]);
        $customer->Active = 1;
        $customer->PlanActive = 1;
        $customer->save();
        
        // send email
        \Yii::$app->mail->compose('welcomeSocialEmail', ['user' => $user, 'password' => $password])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                ->setTo($user->Email)
                ->setSubject(Yii::t('app', 'Welcome to') . ' ' . \Yii::$app->name)
                ->send();
        
        Yii::$app->user->login($user);
    }	
    
    
    
    public function actionLanguage()
    {
		$language = Yii::$app->request->post('language');
		Yii::$app->language = $language;

		$languageCookie = new \yii\web\Cookie([
			'name' => 'language',
			'value' => $language,
			'expire' => time() + 60 * 60 * 24 * 30, // 30 days
		]);
		Yii::$app->response->cookies->add($languageCookie);	
		return $this->goHome();
	}
	
	
    public function actionIndex()
    {
		if (Yii::$app->user->isGuest) {
			return $this->actionLogin();
		}
		
		$customer = Customers::findOne(Yii::$app->user->identity->CustomerID);	
		$session = \Yii::$app->session;
		$session->set('Active', $customer['Active']);
		
		return Yii::$app->runAction('app/index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {						
			$this->event(Yii::$app->user->identity->CustomerID, EVENT_LOGIN, Yii::$app->user->identity->Email);
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	

    public function actionTest() {
                \Yii::$app->mail->compose('activationEmail', 
												['user' => new User()])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                    ->setTo('support@didisoft.com')
                    ->setSubject(Yii::t('app', 'Registration') . ' ' . \Yii::$app->name)
                    ->send();        
    }
    
    
	/**
	* Creates a new customer.
	* The customer registration logic is in SignupForm::signup()
	*/
    public function actionSignup()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) 
			{
                \Yii::$app->mail->compose('activationEmail', 
										['user' => $user, 'password' => $model->password])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                    ->setTo($user->Email)
                    ->setSubject(Yii::t('app', 'Registration') . ' ' . \Yii::$app->name)
                    ->send();

				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Account successfully created. Please check your email to Activate your Registration.'));
				return $this->actionLogin();
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'termsUrl' => $this->getOption(Option_TermsUrl)
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Data saved'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
    public function actionActivate($id, $token)
    {
        if (empty($token) || !is_string($token)) {
			return $this->render('activate', [
				'activated' => false,
			]);
        }
        $user = User::findIdentity($id);
        if (!$user) {
			return $this->render('activate', [
				'activated' => false,
			]);
        }
		
        if ($user->validateAuthKey($token)) 
        {
                $customer = Customers::findOne($user->CustomerID);				
                $customer->Active = 1;
                $customer->PlanActive = 1;
                $customer->save();
                // send email
                \Yii::$app->mail->compose('welcomeEmail', ['user' => $user])
                        ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                        ->setTo($user->Email)
                        ->setSubject(Yii::t('app', 'Welcome to') . ' ' . \Yii::$app->name)
                        ->send();

            if (Yii::$app->getUser()->login($user)) {
                return $this->goHome();
            } else {
                throw new InvalidParamException('User cannot login');				
            }				
        } else {
                return $this->render('activate', [
                        'activated' => false,
                ]);		
        }
    }	
    
	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}	    
}
