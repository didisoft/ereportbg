<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
	
use common\controllers\MyController;
use common\models\Customers;
use common\models\Plans;
use common\models\Orders;
use common\models\Words;
use common\models\Invoices;

class BillingController extends MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['invoice-external', 'bank-print'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    {
		$customer = Yii::$app->user->identity->getCustomer();
		if ($customer->PlanActive < 1) {
			Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Your Subscription plan has expired'));		
		}

		$plan = Plans::findOne($customer->PlanID);
	
        $invoiceProvider = new ActiveDataProvider(['query' => Invoices::find()]);
		$invoiceProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);		
	
        return $this->render('index', [
            'model' => Yii::$app->user->identity,
			'customer' => $customer,
			'plan' => $plan,
			'invoiceProvider' => $invoiceProvider
        ]);
    }

	
	
    public function actionInvoices()
    {
        $invoiceProvider = new ActiveDataProvider(['query' => Invoices::find()]);
		$invoiceProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);		
	
        return $this->render('invoices', [
            'model' => Yii::$app->user->identity,
			'invoiceProvider' => $invoiceProvider
        ]);
    }


	
    public function actionExtend()
    {
		$this->event(Yii::$app->user->identity->CustomerID, EVENT_ORDER_START, 'extend');
		
		$customer = Yii::$app->user->identity->getCustomer();
		
		if ($customer->PlanID == 0) 
		{
			$plan1 = $this->findPlanBySlug('plan1');
			$plan2 = $this->findPlanBySlug('plan2');
			$plan3 = $this->findPlanBySlug('plan3');
			$plan4 = $this->findPlanBySlug('plan4');
		
			return $this->render('plans', [
				'model' => Yii::$app->user->identity,
				'customer' => $customer,
				'plan1' => $plan1,
				'plan2' => $plan2,
				'plan3' => $plan3,
				'plan4' => $plan4,
			]);
		} else {
			// extend existing
			return $this->actionPeriod($customer->PlanID);
		}
    }

	
    public function actionPlan()
    {
		$this->event(Yii::$app->user->identity->CustomerID, EVENT_ORDER_START, 'plan');
	
		$customer = Yii::$app->user->identity->getCustomer();
		
		$plan1 = $this->findPlanBySlug('plan1');
		$plan2 = $this->findPlanBySlug('plan2');
		$plan3 = $this->findPlanBySlug('plan3');
		$plan4 = $this->findPlanBySlug('plan4');
	
		return $this->render('plans', [
			'model' => Yii::$app->user->identity,
			'customer' => $customer,
			'plan1' => $plan1,
			'plan2' => $plan2,
			'plan3' => $plan3,
			'plan4' => $plan4,
		]);
    }


    public function actionPeriod($id)
    {
		$plan = $this->findPlanById($id);	
		$customer = Yii::$app->user->identity->getCustomer();
		
		// on plan change
		$balance = $customer->Balance;
		if ($plan->PlanID != $customer->PlanID) {
			$connection = \Yii::$app->db;
			$model = $connection->createCommand('SELECT datediff(PlanExpirationTime, Now())/30 FROM customers WHERE CustomerID = ' . $customer->CustomerID);
			$months = $model->queryScalar();
		
			$current_plan = $this->findPlanById($customer->PlanID);
			//$balance = $balance + ($months * $plan->
		}
		
        return $this->render('period', [
            'customer' => $customer,
			'balance' => $balance,
			'plan' => $plan,
			'slug' => $plan->slug,
        ]);
    }


	
    public function actionMethod()
    {
		$slug = Yii::$app->request->post('slug');
		$period = Yii::$app->request->post('period');
        return $this->render('method', [
			'period' => $period,
			'slug' => $slug
        ]);
    }

	
	
    public function actionBankStatement()
    {	
		$slug = Yii::$app->request->get('slug');
		$period = Yii::$app->request->get('period');
		
		$plan = $this->findPlanBySlug($slug);			
		$price = $plan->Price;
		if ($period == 3) {
			$price = $plan->Price3m;
		} else if ($period == 6) {
			$price = $plan->Price6m;
		} else if ($period == 12) {
			$price = $plan->Price12m;
		}
		
		$this->event(Yii::$app->user->identity->CustomerID, EVENT_ORDER_END, 'bank:' . $price);
		
		$customer = Yii::$app->user->identity->getCustomer();
		$order = new Orders();
		if ($customer->Balance < $price) {
			$price = $price - $customer->Balance;
			
			$order->BalanceMinus = $customer->Balance;
		} else {
			$order->BalanceMinus = $plan->Price;
		}
				
		// insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = Yii::$app->user->identity->CustomerID;
		$order->PlanID = $plan->PlanID;
		$order->Period = $period;
		$order->Amount = $price;
		$order->MOL = $customer->MOL;
		$order->EIK = $customer->EIK;
		$order->IN_DDS = $customer->IN_DDS;
		$order->Address = $customer->Address;
		$order->CompanyName = $customer->CompanyName;		 
		$order->PaymentMethodID = PAYMENT_METHOD_BANK;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
			return $this->render('bank', [
				'order' => $order,
			]);
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionMethod($slug);
		}	
    }

	
	
	// redirected from ePay on canceled payment
    public function actionEpayCancel()
    {	
		Yii::$app->getSession()->setFlash('error', 'Заявката за плащане бе отказана.');
		return $this->actionIndex();
	}

	
	
	// redirected from ePay on successful payment
    public function actionEpaySuccess()
    {	
		Yii::$app->getSession()->setFlash('info', 'Успешно извършихте плащането. Моля изчакайте няколко минути за да го отрази системата');
		return $this->actionIndex();
	}

	
	
	// redirects to ePay.bg
	// @type := card | login
    public function actionEpayStatement()
    {	
		$slug = Yii::$app->request->get('slug');
		$period = Yii::$app->request->get('period');
		$type = Yii::$app->request->get('type');
		
		$plan = $this->findPlanBySlug($slug);			
		$price = $plan->Price;
		if ($period == 3) {
			$price = $plan->Price3m;
		} else if ($period == 6) {
			$price = $plan->Price6m;
		} else if ($period == 12) {
			$price = $plan->Price12m;
		}

		$this->event(Yii::$app->user->identity->CustomerID, EVENT_ORDER_END, 'epay: ' . $price);
		
		$customer = Yii::$app->user->identity->getCustomer();
		$order = new Orders();
		if ($customer->Balance < $price) {
			$price = $price - $customer->Balance;
			
			$order->BalanceMinus = $customer->Balance;
		} else {
			$order->BalanceMinus = $plan->Price;
		}
				
		// insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = Yii::$app->user->identity->CustomerID;
		$order->PlanID = $plan->PlanID;
		$order->Period = $period;
		$order->Amount = $price;
		$order->MOL = $customer->MOL;
		$order->EIK = $customer->EIK;
		$order->IN_DDS = $customer->IN_DDS;
		$order->Address = $customer->Address;
		$order->CompanyName = $customer->CompanyName;		 
		$order->PaymentMethodID = PAYMENT_METHOD_EPAY;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
			return Yii::$app->runAction('epay/post', ['type' => $type,
													'invoiceId' => $order->OrderID, 
													'price' => $price, 
													'description' => 'Абонамент ' . $plan->Name ]);
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionMethod($slug);
		}	
    }

	
	
    public function actionBankPrint($id)
    {		
		// insert into Orders
		$order = $this->findOrder($id);
		$this->layout = 'blank';
		return $this->render('bankPrint', [
			'order' => $order,
		]);
    }

    public function actionInvoice($id)
    {		
		// data
		$invoice = $this->findInvoice($id);
		$customer = Yii::$app->user->identity->getCustomer();
		//$this->layout = 'blank';
		return $this->render('invoice', [
			'invoice' => $invoice,
			'customer' => $customer,
		]);
    }

    public function actionInvoicePrint($id)
    {		
		// data
		$invoice = $this->findInvoice($id);
		$customer = Yii::$app->user->identity->getCustomer();
		$this->layout = 'blank';
		return $this->render('invoicePrint', [
			'invoice' => $invoice,
			'customer' => $customer,
		]);
    }

	// TODO
    public function actionInvoicePdf($id)
    {		
		// data
		$invoice = $this->findInvoice($id);
		$customer = Yii::$app->user->identity->getCustomer();
		//$this->layout = 'blank';
		
$mpdf=new mPDF('win-1252','A4','','',15,10,16,10,10,10);//A4 page in portrait for landscape add -L.
$mpdf->SetHeader('|Your Header here|');
$mpdf->setFooter('{PAGENO}');// Giving page number to your footer.
$mpdf->useOnlyCoreFonts = true;    // false is default
$mpdf->SetDisplayMode('fullpage');
// Buffer the following html with PHP so we can store it to a variable later
ob_start();
?>
<?php include "phppage.php";
 //This is your php page ?>
<?php 
$html = 		$this->renderPartial('invoice', 
									[
										'invoice' => $invoice,
										'customer' => $customer,
									],
									true);
ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing
$mpdf->WriteHTML($html);
//$mpdf->SetProtection(array(), 'user', 'password'); uncomment to protect your pdf page with password.
$mpdf->Output();
exit;
		
    }


    public function actionInvoiceExternal($id, $key)
    {		
		// data
		$invoice = $this->findInvoice($id);
		if ($invoice != null) {
			$customer = Customers::findOne($invoice->CustomerID);
			if (md5($customer->activkey) === $key) {
				return $this->render('invoice', [
					'invoice' => $invoice,
					'customer' => $customer,
				]);
			} else {
				echo 'Wrong key provided!';
			}
		}
    }
	
    /**
     * Updates an existing Words model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->WordID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	/** in-place diting */
    public function actionEditcompany()
    {
		// Check if there is an Editable ajax request
		if (isset($_POST['hasEditable'])) {
			$model = Yii::$app->user->identity->getCustomer();
			// read your posted model attributes			
			if ($model->load($_POST)) {
				// read or convert your posted information
				if ($model->save()) {				
					$postValues = $_POST[$model->formName()];
					foreach ($postValues as $name => $val) {
						$value = $model->$name;
					}
					
				// return JSON encoded output in the below format
					echo \yii\helpers\Json::encode(['output'=>$value, 'message'=>'']);
				} else {
				// alternatively you can return a validation error
					echo \yii\helpers\Json::encode(['output'=>'', 'message'=>'Validation error']);
				}
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				echo \yii\helpers\Json::encode(['output'=>'', 'message'=>'']);
			}
			return;
		}
    }
	
    protected function findInvoice($id)
    {
		if (Yii::$app->user->isGuest) {
			if (($model = Invoices::findOne(['InvoiceID' => $id])) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		} else {
			if (($model = Invoices::findOne(['InvoiceID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }	

    protected function findOrder($id)
    {
        if (($model = Orders::findOne(['OderID'=>$id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	

    protected function findPlanBySlug($slug)
    {
        if (($model = Plans::findOne(['Slug' => $slug])) !== null) {
            return $model;
        } else {
            return null;
        }
    }	
		
    protected function findPlanById($id)
    {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }		
}
