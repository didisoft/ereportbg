<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Notificationspipe;
use common\models\Appointments;

// - receive phone confirm
// - receive phone reject
// - receive phone callback
class EmailController extends MyController
{
	public function actionCancel()
	{
        $appointment_id = $_REQUEST['id'];
        $key = $_REQUEST['key'];
        $appointment = Appointments::findOne(['AppointmentID' => $appointment_id]);
        if ($appointment != null) {
            if (sha1($appointment['ClientName']) != $key) {
                $this->error('Email sha1 differs id : ' . $appointment_id . ' key: '. $key);
                return;
            }
                        
            $appointment->StatusID = Appointment_Status_Canceled;
            if (!$appointment->save(false)) {
                $this->error($appointment->validate());
            }            
            
            return $this->render('status', ['status' => $this->getOption(Option_EmailCancelText)]);
        } else {
            $this->error('No Email appointment with id : ' . $appointment_id);
        }                
    }
    
    
    
	public function actionConfirm()
	{
        $appointment_id = $_REQUEST['id'];
        $key = $_REQUEST['key'];
        $appointment = Appointments::findOne(['AppointmentID' => $appointment_id]);
        if ($appointment != null) {
            if (sha1($appointment['ClientName']) != $key) {
                $this->error('Email sha1 differs id : ' . $appointment_id . ' key: '. $key);
                return;
            }
                        
            $appointment->StatusID = Appointment_Status_Confirmed;
            if (!$appointment->save(false)) {
                $this->error($appointment->validate());
            }            
            
            return $this->render('status', ['status' => $this->getOption(Option_EmailConfirmText)]);
        } else {
            $this->error('No Email appointment with id : ' . $appointment_id);
        }                
    }
	
}