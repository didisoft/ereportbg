<?php

namespace frontend\controllers;

use Yii;
use common\models\Clients;
use common\models\ClientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends \common\controllers\MyController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actions()
	{
		return [
			'autocomplete' => [
				'class' => 'frontend\controllers\actions\AutocompleteAction',
				'tableName' => Clients::tableName(),
				'field' => 'Name'
			]
		];
	}

    public function actionAutocomplete2() 
    {
        $term = trim(strip_tags(Yii::$app->request->get('term')));//retrieve the search term that autocomplete sends

        $data = Clients::find()
                    ->select(['ClientID as id', 'Name as value', 'Name as label', 'Phone as phone', 'Email as email', 'Mobile as mobile'])
                    ->where(['like', 'Name', $term])
                    ->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID])
                    ->asArray()
                    ->all();

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;		
        
    }
    
    
    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['CustomerID'=>Yii::$app->user->identity->CustomerID]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clients();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $model->CustomerID = Yii::$app->user->identity->CustomerID;
            $model->save(false);
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
