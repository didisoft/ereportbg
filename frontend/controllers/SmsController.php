<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Options;
use common\models\Customers;
use common\models\Appointments;
use common\models\Notificationspipe;

// - receive phone confirm
// - receive phone reject
// - receive phone callback
class SmsController extends MyController
{
    // for fastspring IPN
	public $enableCsrfValidation = false;

    
	//
	//  endpoint for producing call TwiML
	// 
	public function actionSms($id)
	{
		$notification = Notificationspipe::findOne(['ID'=>$id]);
		
        $call_message = '<Sms>' . $notification->SMSText . '</Sms>';        
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');        
		return $this->renderPartial('xml', [
                'content' => $call_message,
            ]);
	}


	public function actionReceive()
	{
        $to = $_REQUEST['To'];
        $number = $_REQUEST['From'];
        $digits = (int)trim($_REQUEST['Body']);

        //$cookies = Yii::$app->request->cookies;
        //$appointmentId = $cookies->getValue('id', '');        
        
        $notificationspipe = NotificationsPipe::find()
                                    ->where(['Mobile' => $number, 
                                    'NotificationType' => Notification_Type_SMS,
                                    'StatusID' => Pipe_Status_Fired])
                                    ->orderBy(['FireTime' => SORT_DESC])
                                    ->one();
        
        $call_message = '';
        if ($notificationspipe != null) {            
            $call_message = Options::getOption(Option_PhoneOptionsText);
                                
            $appointment = Appointments::findOne(['AppointmentID' => $notificationspipe['AppointmentID'] ]);
            $customer = Customers::findOne(['CustomerID' => $appointment->CustomerID]);
            if ($digits == 1) {
                // confirm
                $call_message = $customer->SmsConfirmText;
                $appointment->StatusID = Appointment_Status_Confirmed;
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            } else if ($digits == 5) {
                // cancel
                $call_message = $customer->SmsCancelText;
                $appointment->StatusID = Appointment_Status_Canceled;
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            } else if ($digits == 9) {
                // call you back
                $call_message = $customer->SmsCallbackText;                
                $appointment->StatusID = Appointment_Status_Contact;
                $this->onContactRequest($appointment);
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            }
        } else {
            $this->error('Invalid id received for SMS/Receive:' . $_REQUEST['From']);
        }
        
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');        
		return $this->renderPartial('xml', [
                'content' => '<Response><Message>' .$call_message. '</Message></Response>',
            ]);
    }
    
    
    
	public function actionTest()
	{
	// TODO	
  
    // Twilio REST API version
    $version = "2010-04-01";
 
    // Set our Account SID and AuthToken
    $sid = 'ACb57bccae945bf3372dbb6af76a9d5b62';
    $token = '5c15999452edc56c0ee7a3787691f23c';
     
    // A phone number you have previously validated with Twilio
    $phonenumber = '+359895629405';
     
    // Instantiate a new Twilio Rest Client
    $client = new \Services_Twilio($sid, $token, $version);
 
    try {
        // Initiate a new outbound call
        $call = $client->account->calls->create(
            $phonenumber, // The number of the phone initiating the call
            '+359883322798', // The number of the phone receiving call
            'http://twimlets.com/message?Message%5B0%5D=Kathie%20I%20love%20you%20so%20much!%20You%20are%20my%20best%20friend%20sweatheart!&' // The URL Twilio will request when the call is answered
        );
        echo 'Started call: ' . $call->sid;
    } catch (Exception $e) {
        echo 'Error: ' . $e->getMessage();
    }
	}		
	
}