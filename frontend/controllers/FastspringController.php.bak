<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
	
use common\controllers\MyController;
use common\models\Customers;
use common\models\Plans;
use common\models\Orders;
use common\models\FastspringOrderDetails;

require(__DIR__ . '/../components/FastSpring.php');
//use frontend\components\FsprgSubscription;
//use frontend\components\FsprgSubscriptionUpdate;
//use frontend\components\FsprgException;

class FastspringController extends MyController
{    
	// for fastspring IPN
	public $enableCsrfValidation = false;
	
	private $test = true;
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['activatex', 'deactivatex'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    {
		return $this->actionPlans();
    }

	
	
    public function actionPlan()
    {
		$customer = Yii::$app->user->identity->getCustomer();
		
		$plan1 = $this->findPlanBySlug('plan1');
		$plan2 = $this->findPlanBySlug('plan2');
		$plan3 = $this->findPlanBySlug('plan3');
		$plan4 = $this->findPlanBySlug('plan4');
	
		return $this->render('plans', [
			'model' => Yii::$app->user->identity,
			'customer' => $customer,
			'plan1' => $plan1,
			'plan2' => $plan2,
			'plan3' => $plan3,
			'plan4' => $plan4,
		]);
    }
	
	
	
	public function actionOrder($id) {
		$customer = Yii::$app->user->identity->getCustomer();
		$plan = $this->findPlanById($id);
		$order = new Orders();
				
		// insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = Yii::$app->user->identity->CustomerID;
		$order->PlanID = $plan->PlanID;
		$order->Period = 1;
		$order->Amount = $plan->Price;
		$order->Address = $customer->Address;
		$order->CompanyName = $customer->CompanyName;		 
		$order->PaymentMethodID = PAYMENT_METHOD_FASTSPRING;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
			$plan = $this->findPlanById($id);
			
			$this->redirect(Yii::$app->fastspring->createSubscription($plan->FastSpringProduct, $order->OrderID));
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionPlan();
		}			
	}
	
	

	public function actionUpgrade($id) {

		$customer = Yii::$app->user->identity->getCustomer();
		$plan = $this->findPlanById($id);
		$order = new Orders();
				

        // insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = Yii::$app->user->identity->CustomerID;
		$order->PlanID = $plan->PlanID;
		$order->Period = 1;
		$order->Amount = $plan->Price;
		$order->Address = $customer->Address;
		$order->CompanyName = $customer->CompanyName;		 
		$order->PaymentMethodID = PAYMENT_METHOD_FASTSPRING;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
			$sub = new \frontend\components\FsprgSubscriptionUpdate($customer->FastSpringRef);
			$sub->productPath = '/listen' . $plan->slug;
			
			try {			
				Yii::$app->fastspring->updateSubscription($sub);
				
				$customer->PlanID = $plan->PlanID;
				$customer->save();
				
				return $this->actionPlan();
			} catch (\frontend\components\FsprgException $e) {
				Yii::$app->getSession()->setFlash('error', $e->getMessage() . ' ' . $e->httpStatusCode . ' ' . $e->errorCode);
				$this->error($e->getMessage() . ' ' . $e->httpStatusCode . ' ' . $e->errorCode);
				return $this->actionPlan();				
			}
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionPlan();
		}			
	}


	
	public function actionActivatex()
    {
		if (md5($_REQUEST["security_data"] . Yii::$app->fastspring->activateKey) != $_REQUEST["security_hash"]) {
			$this->error('Bad security hash from FastSpring');
			throw new \yii\web\HttpException(404, 'Bad security hash from FastSpring');
		} else {
		
			$customer_ref = $_POST["SubscriptionReferrer"];
			$subscription_ref = $_POST["SubscriptionReference"];

			if ($customer_ref == null) {
				$this->error('Empty SubscriptionReferrer');
				throw new \yii\web\HttpException(404, 'Bad security hash from FastSpring');
			} else {
				$this->recordPaidOrder($customer_ref, $subscription_ref);
				
                $order = Orders::findOne($id);
                $customer = Customers::findOne($order->CustomerID);
                $customer->FastSpringUrl = $_POST["SubscriptionCustomerUrl"];
                
				$fsOrder = new FastspringOrderDetails();
                $fsOrder->OrderID = $customer_ref;
                $fsOrder->FastSpringRef = $subscription_ref;
                $fsOrder->FastSpringUrl = $_POST["SubscriptionCustomerUrl"];
				$fsOrder->save();
			}		
		}
	}		
	
	
	
	
	public function actionDeactivatex()
    {
		$privatekey = Yii::$app->fastspring->deactivateKey;
		if (md5($_REQUEST["security_data"] . $privatekey) != $_REQUEST["security_hash"]) {
			$this->error('Bad security hash from FastSpring');
			throw new \yii\web\HttpException(404, 'Bad security hash from FastSpring');
		} else {
			$customer_ref = $_POST["SubscriptionReferrer"];

			if ($customer_ref == null) {
				$this->error('Empty SubscriptionReferrer');
				throw new \yii\web\HttpException(404, 'Empty SubscriptionReferrer');
			} else {
				$order = Orders::findOne(['OrderID'=>$customer_ref]);
				$order->OrderStatusID = ORDER_CANCELED;
				$order->save();
				
				$connection = \Yii::$app->db;
				$connection	->createCommand()
							->update('customers', 
									  [
										'PlanActive' => 0,
										'PlanExpirationTime' => new \yii\db\Expression('sysdate()'),
									  ],
									  'CustomerID = ' . $order->CustomerID
									)
							->execute();					
			}
		}		
	}		



    protected function findOrder($id)
    {
        if (($model = Orders::findOne(['OrderID'=>$id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	

    protected function findPlanBySlug($slug)
    {
        if (($model = Plans::findOne(['Slug' => $slug])) !== null) {
            return $model;
        } else {
            return null;
        }
    }	
		
    protected function findPlanById($id)
    {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }		
}
