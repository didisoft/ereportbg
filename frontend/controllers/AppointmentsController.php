<?php
namespace frontend\controllers;

use Yii;
use common\models\Appointments;
use common\models\Schedules;
use common\models\Clients;
use common\controllers\MyController;

use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;


class AppointmentsController extends MyController
{
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Mentions models.
     * @return mixed
     */
    public function actionIndex()
    {	
        $this->layout = '//main1';
        
		$schedules = Schedules::findAll(['CustomerID' => Yii::$app->user->identity->CustomerID, 'Active'=>1]);
                $scheduleIDs = array_map(function($v) {
                                    return $v->ScheduleID;
                                }, $schedules);
		// $appointments = Appointments::find()->where(['in', 'ScheduleID', $scheduleIDs])->all();

        
        // We need custom jQuery version for weekCalendar
        Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
            'sourcePath' => null,
            'basePath' => '@webroot',
            'baseUrl' => '@web',            
            'js' => ['jquery.js' => 'js/jquery-1.7.js'],
        ];

        Yii::$app->assetManager->bundles['yii\jui\JuiAsset'] = [
            'sourcePath' => null,
            'basePath' => '@webroot',
            'baseUrl' => '@web',            
            'js' => ['jquery-ui.js' => 'js/jquery-ui-1.8.11.custom.min.js'],
            'css' => ['jquery-ui.css' => 'js/jquery-ui-1.8.11.custom.css']
        ];
            
        // Bootstrap cannot work with our jQuery
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;

        // Popup for calendar event
        \frontend\assets\ModalAsset::register(Yii::$app->view);
        
        return $this->render('index', [
			'schedules' => $schedules
        ]);
    }

    
    
    public function actionCheckPhone()
    {	        
        if(isset($_REQUEST['Appointments']) && isset($_REQUEST['Appointments']['Mobile'])) {
            $util = \libphonenumber\PhoneNumberUtil::getInstance();

            if ($util->isViablePhoneNumber($_REQUEST['Appointments']['Mobile'])) {            
                return 'true';
            }
        }
        
        return 'false';        
    }
    
    
    
    public function actionLoadCalendar($ids='')
    {	
        $this->layout = 'blank';

        if ($ids !== '') {
            $schedule = Schedules::findOne(['CustomerID' => Yii::$app->user->identity->CustomerID, 'Active'=>1, 'ScheduleID'=>$ids]);
        } else {
            $schedule = Schedules::findOne(['CustomerID' => Yii::$app->user->identity->CustomerID, 'Active'=>1]);
        }
                
        return $this->render('calendar2', ['schedule'=>$schedule]);
    }
    

    
    public function actionLoadCalendarData($ids='')
    {	
        if ($ids !== '') {
            $schedule = Schedules::findOne(['CustomerID' => Yii::$app->user->identity->CustomerID, 'Active'=>1, 'ScheduleID'=>$ids]);
        } else {
            $schedule = Schedules::findOne(['CustomerID' => Yii::$app->user->identity->CustomerID, 'Active'=>1]);
        }
 
        $start = Yii::$app->request->get('start');
        $end = Yii::$app->request->get('end');
        
        $appointments = Appointments::find()->where(['ScheduleID' => $schedule['ScheduleID']])
                ->andWhere(['between', 'StartTime', new \yii\db\Expression('FROM_UNIXTIME('.$start.')'), new \yii\db\Expression('FROM_UNIXTIME('.$end.')')])
                ->asArray()
                ->all();
        $events = [];
        foreach ($appointments as $appointment)
        {
            $events[] = [
                'id' => $appointment['AppointmentID'],
                'status' => $this->getStatusCssClass($appointment['StatusID']),
                'title' => $appointment['ClientName'],
                'start' => (new \DateTime($appointment['StartTime']))->format('Y-m-d H:i:s'),
                'end' => (new \DateTime($appointment['EndTime']))->format('Y-m-d H:i:s'),
                'userId' =>  $schedule['ScheduleID']
             ];                        
        }
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = ['options'=>['timeslotsPerHour'=>(int)(60/$schedule['MinLength'])], 'events'=>$events];

        return $data;
    }

    
    
    public function actionValidateUpdate($id)
    {
        if (Yii::$app->request->isAjax) {
               Yii::$app->response->format = Response::FORMAT_JSON;

               $model = new Appointments();

               if (!$model->validate()) {
                   return ActiveForm::validate($model);
               }
        }       
    }
    
    
    
    /**
     * Creates a new Mentions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->debug(Yii::$app->request);
        if (Yii::$app->request->isAjax) {
            
                if (isset($_POST['delete'])) {
                    $this->debug('delete action');
                    
                    return $this->actionDelete($id);
                }
            
                if (isset($_POST['confirmed']) ||
                        isset($_POST['canceled']) ||
                        isset($_POST['showed']) ||
                        isset($_POST['no_showed'])) {
                    
                    return $this->actionUpdateStatus($id);                    
                }
                
                $model = Appointments::findOne(['AppointmentID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID]);
                if ($model->load(Yii::$app->request->post())) {
                        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        if ($model->save()) {
                            
                            $connection = \Yii::$app->db;
                            $connection->createCommand()->delete('notificationspipe', [
                                'AppointmentID = ' .$id]
                             )->execute();                                                
                            
                            return ['success'];		
                        } else {
                            $this->debug(\yii\widgets\ActiveForm::validate($model));							
                            return \yii\widgets\ActiveForm::validate($model);
                        }
                } else {
                    Yii::$app->assetManager->bundles = FALSE;
                    return $this->renderAjax('_form_ajax_edit', [
                                            'model' => $model
                    ]);                    
                }
        }
    }

    public function actionUpdateStatus($id)
    {
        $this->debug(Yii::$app->request);
        if (Yii::$app->request->isAjax) {
            $model = Appointments::findOne(['AppointmentID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID]);

            if (isset($_POST['confirmed'])) {
                $model->StatusID = Appointment_Status_Confirmed;
            } else if (isset($_POST['canceled'])) {
                $model->StatusID = Appointment_Status_Canceled;
            } else if (isset($_POST['showed'])) {
                $model->StatusID = Appointment_Status_Showed;
            } else if (isset($_POST['no_showed'])) {
                $model->StatusID = Appointment_Status_Not_Showed;
            }

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;    
            if ($model->save()) {
                return ['success',
                    ];		
            } else {
                $this->debug(\yii\widgets\ActiveForm::validate($model));							
                return \yii\widgets\ActiveForm::validate($model);
            }
        }
    }
        
        
    public function actionFind($id)
    {
		if (Yii::$app->request->isAjax) {
			$model = Appointments::findOne(['AppointmentID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID]);
			if ($model != null) {
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return $model;		
			}
		}
    } 

    /**
     * Creates a new Mentions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Appointments();

        if (Yii::$app->request->isAjax) {				
                if ($model->load(Yii::$app->request->post())) {
                        if ($model->ClientID == 0) 
                        {
                            $client = new Clients();
                            $client->Name = $model->ClientName;
                            $client->Phone = $model->Phone;
                            $client->Mobile = $model->Mobile;
                            $client->Email = $model->Email;
                            $client->SendPhone = 0;
                            $client->SendMobile = 1;
                            $client->SendEmail = 1;
                            $client->CustomerID = Yii::$app->user->identity->CustomerID;
                            if ($client->validate()) {
                                $client->save();
                            } else {
                                //throw new \yii\web\HttpException(500, var_export($client->getErrors(), true));
                                return 'error';
                            }

                            $model->ClientID = $client->getPrimaryKey();
                        }
                        $model->StatusID = Appointment_Status_Scheduled;
                        $model->CustomerID = Yii::$app->user->identity->CustomerID;

                        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        if ($model->save()) {
                            return ['success',
                                    'id' => $model->AppointmentID,
                                    'title' => $model->ClientName,
                                    'start' => (new \DateTime($model->StartTime))->format('Y-m-d H:i:s'),
                                    'end' => (new \DateTime($model->EndTime))->format('Y-m-d H:i:s'),
                                    'userId' => $model->ScheduleID,
                                ];		
                        } else {
                            $this->debug(\yii\widgets\ActiveForm::validate($model));
                            return 'error';
                        }
                } else {
					$this->trace('PreLoad');
                        $model->StartTime = Yii::$app->formatter->asDatetime(strtotime(Yii::$app->request->get('start')), 'php:Y-m-d H:i:s');
                        $model->EndTime = Yii::$app->formatter->asDatetime(strtotime(Yii::$app->request->get('end')), 'php:Y-m-d H:i:s');
                        return $this->renderAjax('_form_ajax', [
                                                'model' => $model
                        ]);
                }
        } else {
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->MentionID]);
                } else {
                        return $this->render('create', [
                                'model' => $model,
                        ]);
                }
        }				
    }

    /**
     * Deletes an existing Mentions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $connection = \Yii::$app->db;			
        $connection->createCommand()->delete('notificationspipe', [
            'AppointmentID = ' .$id]
         )->execute();                                                
        
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success'];		
        } else {        
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Mentions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mentions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Appointments::findOne(['AppointmentID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}