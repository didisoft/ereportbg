<?php

namespace frontend\controllers;

use Yii;

use common\controllers\MyController;
use common\models\Notificationspipe;
use common\models\Appointments;
use \common\models\Customers;

// - receive phone confirm
// - receive phone reject
// - receive phone callback
class PhoneController extends MyController
{
    // required for ePay notification
	public $enableCsrfValidation = false;
        
	public function actionCall()
	{
        $id = (int)$_REQUEST['id'];
		$notification = Notificationspipe::findOne(['ID'=>$id]);
		
        $phone_message = $this->formatNotification($notification, $notification['PhoneText']);
        $call_message = '';        
        if (isset($_POST['AnsweredBy']) && $_POST['AnsweredBy'] == 'machine') {
            $call_message = '<Response>'
                . '<Say language="'.\Yii::$app->language.'">' . $phone_message . '</Say>'
            . '</Response>';
        } else { 
            // human
            $call_message = '<Response>'
            . '<Say language="'.\Yii::$app->language.'">' . $phone_message . '</Say>'
            . '<Gather timeout="10" finishOnKey="" numDigits="1" action="'. Yii::$app->urlManager->createAbsoluteUrl(["phone/receive?id=".$notification->AppointmentID]) .'">'
                . '<Say language="'.\Yii::$app->language.'">' . $this->getOption(Option_PhoneOptionsText) . '</Say>' 
            . '</Gather>'
            . '<Say language="'.\Yii::$app->language.'">' . $this->getOption(Option_PhoneOptionsNoChoice) . '</Say>'        
            . '</Response>';
        }

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');        
		return $this->renderPartial('//sms/xml', [
                'content' => $call_message,
            ]);
	}

	public function actionStatus()
	{
        $id = (int)$_REQUEST['id'];
        $CallStatus = $_REQUEST['CallStatus'];
        
        if ($CallStatus == "busy" || 
            $CallStatus == "no-answer" ||
            $CallStatus == "canceled" ||  
            $CallStatus == "failed") {
            
            $notification = Notificationspipe::findOne(['ID'=>$id]);
            // TODO if not notified
        } else if ($CallStatus == "completed") {
            $notification = Notificationspipe::findOne(['ID'=>$id]);
            $appointment = Appointments::findOne(['AppointmentID' => $notification['AppointmentID']]);
            $appointment->StatusID = Appointment_Status_Notified;
            $appointment->save(false);
        } else {
            $this->error('Unexpected CallStatus :' . $CallStatus);
        }
    }
    
	public function actionReceive() {
		// if the caller pressed anything but 1 send them back
        $phone = $_REQUEST['From'];
        $appointment_id = (int)$_REQUEST['id'];
        $digits = (int)$_REQUEST['Digits'];
        
        $call_message = 'Danke für deinen Anruf';

        $appointment = Appointments::findOne(['AppointmentID' => $appointment_id]);
        if ($appointment != null) {
            $customer = Customers::findOne(['CustomerID' => $appointment->CustomerID]);
            if ($digits == 1) {
                // confirm
                $call_message = $customer->PhoneConfirmText;
                $appointment->StatusID = Appointment_Status_Confirmed;
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            } else if ($digits == 5) {
                // cancel
                $call_message = $customer->PhoneCancelText;
                $appointment->StatusID = Appointment_Status_Canceled;
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            } else if ($digits == 9) {
                // call you back
                $call_message = $customer->PhoneCallbackText;
                
                $appointment->StatusID = Appointment_Status_Contact;
                $this->onContactRequest($appointment);                
                if (!$appointment->save(false)) {
                    $this->error($appointment->validate());
                }
            }
        } else {
            $this->error('Invalid id received for Phone/Receive:' . $_REQUEST['id']);
        }
        
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');        
		return $this->renderPartial('//sms/xml', [
                'content' => '<Response><Say language="'.\Yii::$app->language.'">' .
                $call_message .
            '</Say></Response>',
            ]);
	}
	
	public function actionTest()
	{
	// TODO	
  
    // Twilio REST API version
    $version = "2010-04-01";
 
    // Set our Account SID and AuthToken
    $sid = 'ACb57bccae945bf3372dbb6af76a9d5b62';
    $token = '5c15999452edc56c0ee7a3787691f23c';
     
    // A phone number you have previously validated with Twilio
    $phonenumber = '+359895629405';
     
    // Instantiate a new Twilio Rest Client
    $client = new \Services_Twilio($sid, $token, $version);
 
    try {
        // Initiate a new outbound call
        $call = $client->account->calls->create(
            $phonenumber, // The number of the phone initiating the call
            '+359883322798', // The number of the phone receiving call
            'http://twimlets.com/message?Message%5B0%5D=Kathie%20I%20love%20you%20so%20much!%20You%20are%20my%20best%20friend%20sweatheart!&' // The URL Twilio will request when the call is answered
        );
        echo 'Started call: ' . $call->sid;
    } catch (Exception $e) {
        echo 'Error: ' . $e->getMessage();
    }
	}		
	
}