<?php

namespace frontend\controllers;

use Yii;
use common\models\Logins;
use common\models\LoginsSearch;
use common\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * LoginsController implements the CRUD actions for Logins model.
 */
class LoginsController extends MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logins models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoginsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);
		$dataProvider->query->andWhere('LoginID <> ' . Yii::$app->user->identity->id);

		$plan = Yii::$app->user->identity->getCustomer()->getPlan();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'plan' => $plan->one(),
        ]);
    }

    /**
     * Creates a new Logins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Logins();

        if ($model->load(Yii::$app->request->post())) {
			$connection = \Yii::$app->db;		
			$model = $connection->createCommand()
						->insert($model->tableName(), [
								'CustomerID' => Yii::$app->user->identity->CustomerID,
								'FirstName' => $model->FirstName,
								'LastName' => $model->LastName,
								'LastName' => $model->LastName,
								'Email' => $model->Email,
								'Password' => $model->Password,
								'IsAdmin' => $model->IsAdmin,
							])
						->execute();		
			//$model->CustomerID = Yii::$app->user->identity->CustomerID;
			//$model->EnteredBy = Yii::$app->user->identity->LoginID;
			//$model->EnteredOn = new \yii\db\Expression('NOW()');			
			//$model->save(false);
            return $this->actionIndex();		
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Logins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->actionIndex();	
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Logins model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Logins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logins::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
