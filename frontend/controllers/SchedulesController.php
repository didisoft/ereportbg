<?php

namespace frontend\controllers;

use Yii;
use common\models\Schedules;
use common\models\SchedulesSearch;
use common\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SchedulesController implements the CRUD actions for Schedules model.
 */
class SchedulesController extends MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedules models.
     * @return mixed
     */
    public function actionIndex()
    {
		$plan = Yii::$app->user->identity->getCustomer()->getPlan();
	
        $searchModel = new SchedulesSearch();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Schedules::find()->andFilterWhere(['Active' => 1, 'CustomerID'=>Yii::$app->user->identity->CustomerID]),
        ]);
		//$searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->CustomerID);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'plan' => $plan->one()
        ]);
    }

    /**
     * Displays a single Schedules model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schedules model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedules();

        if ($model->load(Yii::$app->request->post())) {
			$connection = \Yii::$app->db;		
			$connection->createCommand()
						->insert($model->tableName(), [
								//'EnteredBy' => Yii::$app->user->identity->LoginID,
								'CustomerID' => Yii::$app->user->identity->CustomerID,
								'Active' => 1,
								'Name' => $model->Name,
								'StartHour' => $model->StartHour,
								'EndHour' => $model->EndHour,
								'MinLength' => $model->MinLength,
							])
						->execute();		
						
			//$this->event(Yii::$app->user->identity->CustomerID, EVENT_ADD_WORD, $model->Name);
			
			//$model->CustomerID = Yii::$app->user->identity->CustomerID;
			//$model->EnteredBy = Yii::$app->user->identity->LoginID;
			//$model->EnteredOn = new \yii\db\Expression('NOW()');			
			//$model->save(false);
            return $this->actionIndex();
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Schedules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditModal($id)
    {
        if (Yii::$app->request->isAjax) {				        
            $model = $this->findModel($id);
            if ($model->load(\Yii::$app->request->post())) {		
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if ($model->save()) {
                    return ['success'];		
                } else {
                    $this->debug(\yii\widgets\ActiveForm::validate($model));
                    return ['error'];
                }
            } else {
                return $this->renderAjax('form_ajax_edit', [
                    'model' => $model,
                ]);
            }
        }
    }

    
    
    /**
     * Updates an existing Schedules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {		
            return $this->actionIndex();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schedules model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->Active = 0;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedules::findOne(['ScheduleID'=>$id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
