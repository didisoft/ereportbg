<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\Words;
use common\models\Orders;
use common\models\Plans;
use frontend\models\Mentions;

use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

define('ONE_SECOND', 1);

class CronController extends MyController
{	
    function actionNotifications() {
        $this->prepareNotifications();
        $this->sendNotifications();
    }
    
    
	function prepareNotifications() {            
            // All times are converted to UTC based on Customer.CountryCode
        
            // CONDITIONS
            // c.PlanActive = 1 - active account
            // c.IntervalMinutesXXX <> 0 - notification Type enabled
            // a.StartTime > utc_timestamp() - event not passed
            // a.IsScheduled = 0 - event still not scheduled
            // IF (c.IntervalMinutesXXX >0, 
            //      date_sub(a.StartTime, INTERVAL c.IntervalMinutesEmail MINUTE), - if > 0 then substract minutes from event.StartTime
            //      ADDTIME(CONVERT(DATE(NOW()), DATETIME), MAKETIME(-c.IntervalMinutesEmail,0,0))) - else (<0) at fixed hour at event date
            
            $sql = 'SELECT ' . Notification_Type_Email . ' as NotificationType,
                        a.*, 
                        c.EmailBody,
                        c.EmailSubject,
                        c.SMSText,
                        c.PhoneText,
                            CONVERT_TZ(
                                IF (c.IntervalMinutesEmail >0, 
                                  date_sub(a.StartTime, INTERVAL c.IntervalMinutesEmail MINUTE), 
                                  ADDTIME(CONVERT(DATE(a.StartTime), DATETIME), MAKETIME(-c.IntervalMinutesEmail,0,0))),
                                c.TimeZone, \'UTC\')
                            as FireTime
                        FROM `appointments` a 
                        INNER JOIN customers c ON a.CustomerID = c.CustomerID
                        WHERE 
                            c.PlanActive = 1
                        AND    
                            c.IntervalMinutesEmail <> 0
                         AND 
                            a.StartTime > utc_timestamp()
                         AND
                            a.IsScheduled = 0';
            
            $sql .= ' UNION ALL ' .
                    'SELECT ' . Notification_Type_SMS . ' as NotificationType,
                        a.*, 
                        c.EmailBody,
                        c.EmailSubject,
                        c.SMSText,
                        c.PhoneText,
                            CONVERT_TZ(
                                IF (c.IntervalMinutesSMS >0, 
                                  date_sub(a.StartTime, INTERVAL c.IntervalMinutesSMS MINUTE), 
                                  ADDTIME(CONVERT(DATE(a.StartTime), DATETIME), MAKETIME(-c.IntervalMinutesSMS,0,0))),
                                c.TimeZone, \'UTC\')  
                            as FireTime
                        FROM `appointments` a 
                        INNER JOIN customers c ON a.CustomerID = c.CustomerID
                        WHERE 
                            c.PlanActive = 1
                        AND    
                            c.IntervalMinutesSMS <> 0
                         AND 
                            a.StartTime > utc_timestamp()
                         AND
                            a.IsScheduled = 0';

            $sql .= ' UNION ALL ' .
                    'SELECT ' . Notification_Type_Phone . ' as NotificationType,
                        a.*, 
                        c.EmailBody,
                        c.EmailSubject,
                        c.SMSText,
                        c.PhoneText,
                            CONVERT_TZ(
                              IF (c.IntervalMinutesPhone >0, 
                                  date_sub(a.StartTime, INTERVAL c.IntervalMinutesPhone MINUTE), 
                                  ADDTIME(CONVERT(DATE(a.StartTime), DATETIME), MAKETIME(-c.IntervalMinutesPhone,0,0))),
                              c.TimeZone, \'UTC\')      
                            as FireTime
                        FROM `appointments` a 
                        INNER JOIN customers c ON a.CustomerID = c.CustomerID
                        WHERE 
                            c.PlanActive = 1
                        AND    
                            c.IntervalMinutesPhone <> 0
                         AND 
                            a.StartTime > utc_timestamp()
                         AND
                            a.IsScheduled = 0';
            
            $connection = \Yii::$app->db;			
            $model = $connection->createCommand($sql);
            $appointments = $model->queryAll();				
            
            foreach ($appointments as $appointment) 
            {
                $customer = Customers::findOne(['CustomerID' => $appointment['CustomerID']]);
                
                $phoneNumberUtil = PhoneNumberUtil::getInstance();
                $mobile_number = '';
                try {
                    $num = $phoneNumberUtil->parse($appointment['Mobile'], $customer->CountryCode);
                    $mobile_number = $phoneNumberUtil->format($num, PhoneNumberFormat::E164);
                } catch (\libphonenumber\NumberParseException $e) {
                    $this->error($e->getMessage() . ';Mobile number invalid ' . $appointment['Mobile'] . '; ClientID ' . $appointment['ClientID']);                    
                    if ($appointment['NotificationType'] == Notification_Type_SMS) 
                    { 
                        continue; // skip SMS if number invalid
                    }
                }
                
                $phone_number = '';
                try {
                    $num = $phoneNumberUtil->parse($appointment['Phone'], $customer->CountryCode);
                    $phone_number = $phoneNumberUtil->format($num, PhoneNumberFormat::E164);
                } catch (\libphonenumber\NumberParseException $e) {
                    $this->error($e->getMessage() . ';Phone number invalid ' . $appointment['Phone'] . '; ClientID ' . $appointment['ClientID']);                    
                    if ($appointment['NotificationType'] == Notification_Type_Phone) 
                    { 
                        $this->error($appointment['Phone'] . ';Phone number invalid ');
                        continue; // skip Phone if number invalid
                    }
                }
                                                
                $connection->createCommand()->insert('notificationspipe', [
                    'ClientID' => $appointment['ClientID'],
                    'FireTime' => $appointment['FireTime'],
                    'StatusID' => Pipe_Status_Pending,
                    'NotificationType' => $appointment['NotificationType'],
                    'Email' => $appointment['Email'],
                    'Mobile' => $mobile_number,
                    'Phone' => $phone_number,
                    'EmailSubject' => $appointment['EmailSubject'],
                    'EmailBody' => $appointment['EmailBody'],
                    'SMSText' => $appointment['SMSText'],
                    'PhoneText' => $appointment['PhoneText'],
                    'AppointmentID' => $appointment['AppointmentID'],
                    'AppointmentStartTime' => $appointment['StartTime'],
                    'AppointmentEndTime' => $appointment['EndTime'],
                    'CustomerID' => $appointment['CustomerID']
                 ])->execute();                
                
                $connection->createCommand()->update('appointments', [
                    'IsScheduled' => 1],
                    'AppointmentID = ' .$appointment['AppointmentID']
                 )->execute();                                
            }
            
	}			
        
	function sendNotifications() {
        // for each notification
        // if email send email
        // if sms send sms
        // if phone send phone
        // mark as done
        //Yii::$app->twillio->call($number, $message);

        $connection = \Yii::$app->db;			

        $sql = '    select * from notificationspipe np'
                . ' inner join appointments a on a.AppointmentID = np.AppointmentID '
                . ' where a.StatusID <= ' . Appointment_Status_Notified                
                . ' and np.FireTime <= UTC_TIMESTAMP()'
                . ' and np.AppointmentStartTime >= UTC_TIMESTAMP()'
                . ' and np.StatusID = ' . Pipe_Status_Pending;

        $model = $connection->createCommand($sql);
        $sources = $model->queryAll();				
        foreach ($sources as $notification) 
        {	                        
          $transaction = $connection->beginTransaction();

          try {
            if ($notification['NotificationType'] == Notification_Type_Phone) {                    
                $messageUrl = Yii::$app->urlManager->createAbsoluteUrl(["phone/call", "id" => $notification['ID']]);
                $statusUrl = Yii::$app->urlManager->createAbsoluteUrl(["phone/status", "id" => $notification['ID']]);

                Yii::$app->twillio->call($notification['Phone'], $messageUrl, $statusUrl);
                
                // By default each Twilio account can make 1 outgoing call per-second. 
                sleep(ONE_SECOND);                
            } else if ($notification['NotificationType'] == Notification_Type_SMS) {
                $sms_message = $this->formatSms($notification); 
                Yii::$app->twillio->sms($notification['Mobile'], $sms_message);                        
                
                // By default each Twilio account can make 1 outgoing call per-second. 
                sleep(ONE_SECOND);                
            } else {
                $this->sendEmail($notification);
            }

            // mark appointment as Scheduled
            $connection->createCommand()->update('appointments', [
                'StatusID' => Appointment_Status_Notified],
                'AppointmentID = ' .$notification['AppointmentID']
             )->execute();                               

            // mark as Pipe_Status_Fired
            $connection->createCommand()->update('notificationspipe', [
                'StatusID' => Pipe_Status_Fired],
                'ID = ' .$notification['ID']
             )->execute();                                                

            $transaction->commit();                
          } catch (\Exception $e) {  
               $transaction->rollBack();
               throw $e;
          }              
        }            
	}			        

    
    
    function sendEmail($notification) {
        $subject = $this->formatNotification($notification, $notification['EmailSubject']);
        $message = $this->formatNotification($notification, nl2br($notification['EmailBody']));

        $clientName = Yii::$app->db->createCommand('SELECT ClientName FROM appointments WHERE AppointmentID = ' . $notification['AppointmentID'])
                                    ->queryColumn();        
        
        $links = Yii::t('app', 'To confirm your appointment, please go to') . ' ' .
                Html::a(Yii::$app->urlManager->createAbsoluteUrl(["email/confirm", "id" => $notification['AppointmentID'], 'key' => sha1($clientName[0])]),
                        Yii::$app->urlManager->createAbsoluteUrl(["email/confirm", "id" => $notification['AppointmentID'], 'key' => sha1($clientName[0])]));
        $links .= '<br/><br/>';
        $links .= Yii::t('app', 'To cancel your appointment, please go to') . ' ' .
                Html::a(Yii::$app->urlManager->createAbsoluteUrl(["email/cancel", "id" => $notification['AppointmentID'], 'key' => sha1($clientName[0])]),
                        Yii::$app->urlManager->createAbsoluteUrl(["email/cancel", "id" => $notification['AppointmentID'], 'key' => sha1($clientName[0])]));

        \Yii::$app->mail->compose(['html' => 'notificationEmail'],
                ['content' => $message,
                    'antispam' => $this->getOption(Option_EmailAntispamText),
                    'links' => $links,
                    'title'=> $subject]
                )
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
            ->setTo($notification['Email'])
            ->setSubject($subject)
            ->send();                            
    }
    
    
    
    function actionTestEmail() {
           $sql = 'select * from notificationspipe np'
                    . ' where np.ID = ' . 8;
            
            $connection = \Yii::$app->db;			
            $model = $connection->createCommand($sql);
            $sources = $model->queryAll();				
            
            $this->debug($sources);
            foreach ($sources as $notification) 
            {	
                $this->sendEmail($notification);
            }        
    }
    
    function actionTestSms() {
           $sql = 'select * from notificationspipe np'
                    . ' where np.ID = ' . 8;
            
            $connection = \Yii::$app->db;			
            $model = $connection->createCommand($sql);
            $sources = $model->queryAll();				
            
            $this->debug($sources);
            foreach ($sources as $notification) 
            {	
                $this->sendEmail($notification);
            }        
    }
    
    
    function actionTestCall() {
           $sql = 'select * from notificationspipe np where np.ID = ' . 8;
            
            $connection = \Yii::$app->db;			
            $model = $connection->createCommand($sql);
            $sources = $model->queryAll();				
            
            foreach ($sources as $notification) 
            {	
                if ($notification['SendPhone']) {
                    // send sms
                    $messageUrl = 'http://8db49537.ngrok.io' . Yii::$app->urlManager->createUrl(["phone/call", "id" => $notification['ID']]);
                    $statusUrl = 'http://8db49537.ngrok.io' . Yii::$app->urlManager->createUrl(["phone/status", "id" => $notification['ID']]);
                                        
                    try {
                        Yii::$app->twillio->call($notification['Phone'], 
                                                $messageUrl,
                                                $statusUrl);      
                        // todo status callback
                    } catch (Exception $e) {
                        $this->error($e);
                    }                    
                } 
            }        
    }
 
    
   function actionTestNumber() {
        echo  Yii::$app->twillio->isMobileNumber('+359895629405', 'BG');      
    }    
}