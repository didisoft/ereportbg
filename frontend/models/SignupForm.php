<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

use common\models\User;
use common\models\Customers;
use common\models\Plans;
use common\models\Options;
use common\models\Schedules;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $companyName;
    public $countryCode;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'countryCode', 'companyName'], 'safe'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This Email is already registered')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Your Name'),
            'companyName' => Yii::t('app', 'Company Name'),
            'countryCode' => Yii::t('app', 'Country'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
        ];
    }
	
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {	
        if ($this->validate()) {
			$trialPlan = Plans::findOne(['slug' => 'trial']);	
		
			$customer = new Customers();
			$customer->Active = 0;
			$customer->PlanID = $trialPlan->PlanID;
            $customer->PlanActive = 1;
            $customer->CompanyName = $this->companyName;
			$customer->RegistrationTime = new \yii\db\Expression('NOW()');
			$customer->PlanExpirationTime = new \yii\db\Expression('date_add( NOW(), interval 15 day)');
			$customer->activkey = sha1(microtime().$this->password);
            
            $customer->SMSText = Options::getOption(Option_SmsText);
            $customer->SmsConfirmText = Options::getOption(Option_SmsConfirmText);
            $customer->SmsCancelText = Options::getOption(Option_SmsCancelText);
            $customer->SmsCallbackText = Options::getOption(Option_SmsCallbackText);
            $customer->PhoneConfirmText = Options::getOption(Option_PhoneConfirmText);
            $customer->PhoneText = Options::getOption(Option_PhoneText);
            $customer->PhoneCancelText = Options::getOption(Option_PhoneCancelText);
            $customer->PhoneCallbackText = Options::getOption(Option_PhoneCallbackText);
            $customer->EmailSubject = Options::getOption(Option_EmailSubject);
            $customer->EmailBody = Options::getOption(Option_EmailBody);
            $customer->EmailConfirmText = Options::getOption(Option_EmailConfirmText);
            $customer->EmailCancelText = Options::getOption(Option_EmailCancelText);
            $customer->MinLength = 15; // minutes
            $customer->StartHour = 8;
            $customer->EndHour = 19;
            $customer->IntervalMinutesEmail = 24*60; // see NotificationHelper::notificationValues()
            $customer->IntervalMinutesSMS = -8;
            $customer->IntervalMinutesPhone = 60;
            $customer->CountryCode = $this->countryCode;
            $customer->TimeZone = Options::getOption(Option_DefaultTimeZone);
			$customer->Discount = 0;

            if ($customer->save(false)) {			
                $user = new User();
				$user->CustomerID = $customer->getPrimaryKey();
                $names = explode(' ', $this->name, 2);
                $user->FirstName = $names[0];
                if (sizeof($names) > 1) {
                    $user->LastName = $names[1];
                }
				$user->Email = $this->email;
				$user->setPassword($this->password);
				$user->generateAuthKey();
				$user->IsAdmin = 1;
				$user->save(false);
                
                $schedule = new Schedules();
                $schedule->CustomerID = $customer->getPrimaryKey();
                $schedule->Name = Options::getOption(Option_DefaultScheduleName);
                $schedule->MinLength = $customer->MinLength;
                $schedule->StartHour = $customer->StartHour;
                $schedule->EndHour = $customer->EndHour;
                $schedule->Active = 1;
                $schedule->save(false);
                                
				return $user;
            }
        }

        return null;
    }    
}
