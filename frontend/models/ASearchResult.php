<?php 
namespace frontend\models;

use yii\base\Model;

class ASearchResult extends Model
{
	public $sourceId;
	public $title;
	public $url;
	public $snippet;	
}
?>