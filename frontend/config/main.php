<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
	'name' => 'ErinnerungsBot',
    'basePath' => dirname(__DIR__),
	'language' => 'de',
    'bootstrap' => ['log', 'debug',
		[
            'class' => 'frontend\components\LanguageSelector',
            'supportedLanguages' => ['de'],
        ]
	],
    'controllerNamespace' => 'frontend\controllers',	
	'modules'=> [
		'debug' => [
		 'class' => 'yii\debug\Module',
		 'allowedIPs' => [ '127.0.0.1', '::1']
		]
	],
	
    'components' => [
        'twillio' => [
            'class' => 'frontend\components\Twillio',
            'sid' => $params['twilio_sid'],
            'token' => $params['twilio_token']
        ],
        'fastspring' => [
            'class' => 'frontend\components\FastSpring',
            'store_id' => $params['fastspring_store'],
            'api_username' => $params['fastspring_user'],
            'api_password' => $params['fastspring_pass'],
            'test_mode' => $params['fastspring_test'],
            'activateKey' => $params['fastspring_private_key_activate'],
            'deactivateKey' => $params['fastspring_private_key_deactivate']
        ],
		'authClientCollection' => [
				'class' => 'yii\authclient\Collection',
				'clients' => [
                    'google' => [
                                    'class' => 'yii\authclient\clients\GoogleOAuth',
                                    'clientId' => $params['google_clientId'],
                                    'clientSecret' => $params['google_clientSecret'],
                                 ],                    
					'facebook' => [
									'class' => 'yii\authclient\clients\Facebook',
									// Facebook login form will be displayed in 'popup' mode
                                    //'authUrl' => 'https://www.facebook.com/dialog/oauth',
									'clientId' => $params['facebook_clientId'],
									'clientSecret' => $params['facebook_clientSecret'],
                                    'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
								],					
				],
		],		
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
		'i18n' => [
            'class' => common\components\I18N::className(),
			'translations' => [
				'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'messageTable' => 'message',
                    'sourceMessageTable' => 'source_message',
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'missingTranslation']
				],
			],
            'languages' => ['en', 'de']
		],		
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
