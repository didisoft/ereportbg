<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MentionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Appointments');


$this->blocks['content-header'] = 
        Yii::t('app', 'Schedule') . ': <select id="data_source">';
foreach($schedules as $schedule) 
{
    $this->blocks['content-header'] .= '<option value="'. $schedule->ScheduleID .'">'. $schedule->Name .'</option>';
}
$this->blocks['content-header'] .= '</select>';
$this->blocks['content-header'] .= '&nbsp;<div class="hint btn btn-info" style="line-height: inherit;" data-tooltip-content="#tooltip_content">?</div>';
$this->blocks['content-header'] .= '<div class="tooltip_templates">';
$this->blocks['content-header'] .= '<span id="tooltip_content">';
$this->blocks['content-header'] .= nl2br(Yii::t('app', "A schedule represents one resource - a room, service provider, machine, etc -- whose appointments need to be tracked."));
$this->blocks['content-header'] .= '</span>';
$this->blocks['content-header'] .= '</div>';
?>

<style>
.ui-autocomplete
{
position:absolute;
cursor:default;
z-index:1001 !important
}
.tooltip_templates {display: none;}
</style>

<div id="modalNew" class="modal remote fade" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
   <?php
echo \Yii::$app->view->render('@frontend/views/app/_form_ajax', ['model'=> new \common\models\Appointments()]);
    ?>
		</div>
	</div>    
</div>
<div id="modalEdit" class="modal remote fade" role="dialog"></div>    

<div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

    <div class="info-box-content">
      <span>
          <?= Yii::t('app', 'Welcome! Your account is ready to use. You can take the tour, which will walk you through how to set up appointments and reminders. It takes about five minutes. You will need to have a phone.') ?>
          <br/><br/>
          <a class="btn btn-info" target="video" href="https://youtu.be/2eeogwP6Z2A">Video</a>
          <?= Yii::t('app', 'This message will disappear in a few days, but you can always retake the tour through the Account screen.') ?> 
        </span>
    </div>
</div>

<?php
      if (!Yii::$app->user->identity->getCustomer()->CompanyName) {
?>
    <div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
      <span>
          <?php
                    $hintText = Yii::t('app', 'Company Name must be filled');
                    $hintText .= '&nbsp;';
                    $hintText .= Html::a(Yii::t('app', 'Edit'), ['settings/company'], ['class' => 'btn-sm btn-success']);
                    echo $hintText;
           ?>               
        </span>
    </div>
<?php
    }
?>               
                    


<?php 
$js = 'var g_urlCalendar = "'. Yii::$app->urlManager->createAbsoluteUrl(["app/load-calendar?ids="]) . '";';
$js .= 'var g_urlScheduleEdit = "'. Yii::$app->urlManager->createAbsoluteUrl(["schedules/edit-modal?id="]) . '";';
$js .= <<<EOT
submitNewForm = function() {
	var _form = $('#create-form');
    $.ajax({
         url: _form.attr('action'),
         type: 'post',
         data: _form.serialize(),
         success: function (response) {
               $('#modalNew').modal('hide');
               $('#calendar').fullCalendar( 'refetchEvents' );	
         },
    });
}
                
$('#modalNew').on('submit', 'form', function(event) {
    event.preventDefault();        
    submitEditForm();        
    return false;
});
        
   
        
$("#modalNew").on("hidden.bs.modal", function () {
    $('#calendar').fullCalendar('unselect');
});
	        
$('#modalNew').on('shown.bs.modal', function () {
    $('#appointments-clientname').focus();
});  
        
$('#modalEdit').on('shown.bs.modal', function () {
    $('#cname').focus();
});  

   
        
var showHideFired = false;
showHideEvents = function(css, _checkbox) {
	showHideFired = true;
	if (_checkbox.is(':checked')) {
		$('.' + css).not('.external-event').show();		
	} else {
		$('.' + css).not('.external-event').hide();		
	}	
}
        
        
$('.external-event').on('click', function() {	
	if (showHideFired) {
		showHideFired = false;
	} else {
		_checkbox = $(this).find("input:first");	
		css = $(this).attr('role');
		
		_checkbox.prop('checked', !_checkbox.is(':checked'));
		showHideEvents(css, _checkbox);
		showHideFired = false;
	}
});


        
$('#data_source').change(function() {	
	$('#calendar').fullCalendar( 'removeEvents' );
	$('#calendar').fullCalendar( 'removeEventSource', g_currentSourceUrl );
	
	g_currentSourceUrl = BaseUrl + 'load-calendar-data?ids=' + jQuery('#data_source').val();
	$('#calendar').fullCalendar( 'addEventSource', 
		{
		  url: g_currentSourceUrl
	    });
});        

$('.hint').tooltipster();   
        
EOT;

///////////////////////////////
//
// Load calendar with delay
// 
///////////////////////////////
$js .= "setTimeout(function() {        
jQuery.ajax({
  url: g_urlCalendar,
  dataType: 'script',
  success : function(request) {
    ". 
       // support for goto calendar event 
       ($gotoId != null ? 
        "$('#calendar').fullCalendar( 'gotoDate', '" . (new \DateTime($gotoId->StartTime))->format('Y-m-d') . "' ); " . 
        // show event edit dialog        
        "jQuery.ajax({
        data:'id=' + ".$gotoId->AppointmentID.", 
        success:function(request){ 
                $('#modalEdit').html(request); 
                $('#modalEdit').modal('show');
        }, 
        type:'post', 
        url: '" . Yii::$app->urlManager->createAbsoluteUrl(["app/update", "id" => $gotoId->AppointmentID]) . "'});"                
        : "") .
  "}
 });                
}, 100);";                        

?>

<?= Yii::$app->language ?>
<div id="message" class="ui-corner-all"></div>

<div class="row">
		
  <div class="col-md-9">
	<div class="box box-primary">
            <div class="box-body no-padding">
				<div id="calendar"></div>
			</div>
	</div>
  </div>

  <div class="col-md-3">
  
          <div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title"><?= Yii::t('app', 'Color codes') ?></h4>
              <span class="hint btn-xs btn-info" style="float:right" data-tooltip-content="#tooltip_content_colors">?</span>
              <div class="tooltip_templates">
                  <span id="tooltip_content_colors">
                <?= nl2br(Yii::t('app', "Scheduled: The client has not been contacted yet. \n"
                      . "Confirmed: The client has confirmed that they are coming to the appointment.\n"
                      . "Notified: We contacted the client and left a message, but they haven't replied yet.\n"
                      .  "Requested Contact: When we contacted the client, they requested to speak to you about the appointment.\n"                      
                      .  "Canceled: The client canceled the appointment.\n")) ?>                  
                  </span>   
              </div>
            </div>
            <div class="box-body">
              <div id="external-events">
                <div class="external-event scheduled" role="scheduled" style="position: relative; color:#ffffff; cursor: default;"><input type="checkbox" onclick="showHideEvents('scheduled', $(this))" checked> <?= Yii::t('app', 'Scheduled') ?></div>
                <div class="external-event notified" role="notified" style="position: relative; color:#ffffff; "><input type="checkbox" onclick="showHideEvents('notified', $(this))" checked> <?= Yii::t('app', 'Notified') ?></div>
                <div class="external-event confirmed" role="confirmed" style="position: relative; color:#ffffff; "><input type="checkbox" onclick="showHideEvents('confirmed', $(this))" checked> <?= Yii::t('app', 'Confirmed') ?></div>
                <div class="external-event canceled" role="canceled" style="position: relative; color:#ffffff; "><input type="checkbox" onclick="showHideEvents('canceled', $(this))" checked> <?= Yii::t('app', 'Canceled') ?></div>
                <div class="external-event requested_contact" role="requested_contact" style="position: relative; color:#ffffff; "><input type="checkbox" onclick="showHideEvents('requested_contact', $(this))" checked> <?= Yii::t('app', 'Requested Contact') ?></div>
              </div>
            </div>
          </div>
		  
		<div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title"><?= Yii::t('app', 'Calendar Properties') ?></h4>
              <?= Html::a(Yii::t('app', 'Edit'), ['settings/calendar-preferences' ], ['class' => 'btn-xs btn-info']) ?>                        
              <span class="hint btn-xs btn-info" style="float:right" data-tooltip-content="#tooltip_content_calendar">?</span>
              <div class="tooltip_templates">
                  <span id="tooltip_content_calendar">
                  <?= nl2br(Yii::t('app', "You can configure what days and times are available for the Calendar")) ?>                  
                  </span>   
              </div>              
            </div>
            <div class="box-body">
             <div class="col-md-6">              
				<b><?= Yii::t('app', 'Start Time') ?></b> <?= $customer->StartHour ?>:00
             </div>    
             <div class="col-md-6"> 
				<b><?= Yii::t('app', 'End Time') ?></b> <?= $customer->EndHour ?>:00
             </div>    
             <div class="col-md-12">   
                <b><?= Yii::t('app', 'Time Slots') ?></b> <?= gmdate("i:s", $customer->MinLength*60); //\Yii::t('app', '{0,duration}', $customer->MinLength*60); ?>
             </div>    
            </div>
          </div>

         <?php
            $values = \common\helpers\NotificationHelper::notificationValues();
         ?> 
		<div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title"><?= Yii::t('app', 'Notifications') ?></h4>
              <?= Html::a(Yii::t('app', 'Edit'), ['settings/notification-preferences' ], ['class' => 'btn-xs btn-info']) ?>                        
              <span class="hint btn-xs btn-info" style="float:right" data-tooltip-content="#tooltip_content_notifications">?</span>
              <div class="tooltip_templates">
                  <span id="tooltip_content_notifications">
                  <?= nl2br(Yii::t('app', "Our system will automatically reminds customers in advance of their appointment. You can control when you want this to happen.")) ?>                  
                  </span>   
              </div>              
            </div>
            <div class="box-body">
             <div class="col-md-12">              
                 <b><?= Yii::t('app', 'Email') ?></b> <?= $values[$customer->IntervalMinutesEmail] ?>
             </div>    
             <div class="col-md-12"> 
				<b><?= Yii::t('app', 'SMS') ?></b> <?= $values[$customer->IntervalMinutesSMS] ?>
             </div>    
             <div class="col-md-12"> 
				<b><?= Yii::t('app', 'Phone') ?></b> <?= $values[$customer->IntervalMinutesPhone] ?>
             </div>    
            </div>
          </div>
      
  </div>  
</div>

  
<?php 
$this->registerJs($js, $this::POS_READY);
?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/scripts.js'); ?>

