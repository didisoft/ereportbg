var BaseUrl = "<?= Yii::$app->request->baseUrl . '/app/' ?>";

onDragResize = function(event, delta, revertFunc, jsEvent, ui, view) {
	var start = moment(event.start);
	var end = moment(event.end);
	jQuery.ajax({
			  success:function(request){ 
					// do nothing
			  }, 
			  error:function(request){ 
				revertFunc();
			  },
			  data:'Appointments[StartTime]=' + start.toISOString() + '&Appointments[EndTime]=' + end.toISOString() + '&Appointments[ScheduleID]=' + 1, 
			  type:'post', 
			  url: BaseUrl + 'update?id='+event.id
		});			
}	

var g_currentEvent = null;

var calendar = jQuery('#calendar').fullCalendar({
    lang: '<?= Yii::$app->language ?>',
	contentHeight: "auto",
	defaultView : "agendaWeek",
    allDaySlot : false,    
    minTime : "<?= $customer->StartHour ?>:00:00",    
    maxTime : "<?= $customer->EndHour ?>:00:00",    
    slotDuration : "00:<?= str_pad($customer->MinLength, 2) ?>:00",    
	slotEventOverlap: false,
	header: {
        left: "prev,next today",
        center: "title",
        right: "agendaWeek,month"
      },
	buttonText: {
        today: "<?= Yii::t('app', 'today') ?>",
        week: "<?= Yii::t('app', 'week') ?>",
        month: "<?= Yii::t('app', 'month') ?>"
      },	  
    editable: true,
    droppable: true,	  
	selectable: true,
	selectHelper: true,
	select: function(start, end, allDay) {
		var userId = $('#data_source').val();
		$('#create-form').prop('action', BaseUrl + 'create');
		$('#create-form').yiiActiveForm('resetForm');
		$('#appointments-clientname').val('');
		$('#appointments-phone').val('');
		$('#appointments-mobile').val('');
		$('#appointments-email').val('');
		$('#appointments-starttime').val(start.toISOString());
		$('#appointments-endtime').val(end.toISOString());
		$('#appointments-scheduleid').val(userId);
        
        $('#appointments-label-event-time').html('<b><?= Yii::t('app', 'Date') ?></b>' +': '+ start.locale('<?= Yii::$app->language ?>').format('MMMM Do YYYY, h:mm a') + ' - ' + end.locale('<?= Yii::$app->language ?>').format('h:mm a'));
        
		$('#modalNew').modal('show');
	},	
		
	eventClick: function(calEvent, jsEvent, view) {
		g_currentEvent = calEvent;
		jQuery.ajax({
			data:"id=" + calEvent.id, 
			success:function(request){ 
					$('#modalEdit').html(request); 
					$('#modalEdit').modal('show');
			}, 
			type:'post', 
			url: BaseUrl + 'update?id=' + calEvent.id});        
    },

	eventResize: onDragResize,	
	eventDrop: onDragResize
});


var g_currentSourceUrl = BaseUrl + 'load-calendar-data?ids=' + $('#data_source').val();
$('#calendar').fullCalendar( 'addEventSource', 
{
  url: g_currentSourceUrl
});



onUpdateEvent = function(isDelete, response) {
	if (g_currentEvent != null) {
		if (isDelete) {
			$('#calendar').fullCalendar( 'removeEvents', g_currentEvent.id );	
		} else {
            g_currentEvent.className = response['className'];
			$('#calendar').fullCalendar( 'updateEvent', g_currentEvent );	
		}
	}
}