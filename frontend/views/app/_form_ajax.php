<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = 
            ActiveForm::begin([
                   'options' => [
                    'id' => 'create-form',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form', 
                    'enableAjaxValidation' => true,	
                ]]);    
	?>

	  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= Yii::t('app', 'New Appointment') ?></h4>
      </div>
	  <div class="modal-body">
	  <?= $form->errorSummary($model); ?>        

        <input type="hidden" id="ClientID" name="Appointments[ClientID]" value="0"/>
        <?= $form->field($model, 'ScheduleID', ['template' => '{input}'])->hiddenInput() ?>
        <?php 
        $data = common\models\Clients::find(['CustomerID' => Yii::$app->user->identity->CustomerID])
    ->select(['concat(Name, \', \', Email) as label', 'Name as value', 'Phone', 'Mobile', 'Email', 'ClientID as id'])
    ->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID])
    ->orderBy('Name')
    ->asArray()
    ->all();

echo $form->field($model, 'ClientName')->widget(AutoComplete::classname(), [
    'options' => ['class' => 'ui-front'],
    'clientOptions' => [
        "appendTo" => "#create-form",
      'source' => $data,
      'autoFill'=>true,
      'minLength'=>'3',
      'select' => new JsExpression("function( event, ui ) {
        $('#appointments-phone').val(ui.item.Phone);
        $('#appointments-mobile').val(ui.item.Mobile);
        $('#appointments-email').val(ui.item.Email);
        $('#ClientID').val(ui.item.id);
      }")
    ],
  ]);
 ?>              
        <?= $form->field($model, 'Phone',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Mobile',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Email',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                

        <div id="appointments-label-event-time">            
        </div>
        <div>
            <i><?= Yii::t('app', 'You can change the time or duration by dragging the appointment after saving')?></i>
        </div>
        <?= $form->field($model, 'StartTime', ['template' => '{input}'])->hiddenInput() ?>                
        <?= $form->field($model, 'EndTime', ['template' => '{input}'])->hiddenInput() ?>                
	  </div>
      <div class="modal-footer">
       <?= Html::button(Yii::t('app', 'OK'), ['onclick' => "submitNewForm(); return true;", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Cancel') ?></button>
      </div>        
    <?php 
        ActiveForm::end(); 
    ?>

