<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= Yii::t('app', 'Edit Appointment') ?></h4>
      </div>
	  <div class="modal-body">
          
    <?php $form = 
            ActiveForm::begin([
                'validationUrl' => Url::to(['app/validate']),
                'enableAjaxValidation' => false,                    
                'options' => [
                    'id' => 'edit-form',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form ui-front', 
                    ],
                ]);    
	?>

            
        <?= $form->errorSummary($model); ?>        

        <input type="hidden" id="ClientID" name="Appointments[ClientID]" value="0"/>
        <?= $form->field($model, 'ScheduleID', ['template' => '{input}'])->hiddenInput() ?>
<?php
$data = common\models\Clients::find()
    ->select(['Name as label', 'Name as value', 'Phone', 'Mobile', 'Email', 'ClientID as id'])
    ->asArray()
    ->all();

echo $form->field($model, 'ClientName')->widget(AutoComplete::classname(), [
    'options' => ['class' => 'ui-front', 'id' => 'cname'],
    'clientOptions' => [
        "appendTo" => "#edit-form",
      'source' => $data,
      'autoFill'=>true,
      'minLength'=>'3',
      'select' => new JsExpression("function( event, ui ) {
        $('#appointments-phone').val(ui.item.Phone);
        $('#appointments-mobile').val(ui.item.Mobile);
        $('#appointments-email').val(ui.item.Email);
        $('#ClientID').val(ui.item.id);
      }")
    ],
  ]);
?>
        <?= $form->field($model, 'Phone',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                    'enableAjaxValidation' => true
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Mobile',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Email',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                

        <div id="appointments-label-event-time">            
            <b><?= Yii::t('app', 'Date') ?></b> :
            <?= (new \DateTime($model->StartTime))->format('F jS Y H:i') ?> - <?= (new \DateTime($model->EndTime))->format('H:i') ?>
        </div>
        <div>
            <i><?= Yii::t('app', 'You can change the time or duration by dragging the appointment after saving')?></i>
        </div>
        <?= $form->field($model, 'StartTime', ['template' => '{input}'])->hiddenInput() ?>                
        <?= $form->field($model, 'EndTime', ['template' => '{input}'])->hiddenInput() ?>                
        
	  </div>
      <div class="modal-footer">
        <?= Html::button(' ' . Yii::t('app', 'Bestätigt'), ['name'=>'confirmed', 'value'=>'confirmed', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'confirmed'}).appendTo('#edit-form'); submitEditForm(); return true;", 'class' => 'btn confirmed']) ?>
        <?= Html::button(' ' . Yii::t('app', 'Abgesagt'), ['name'=>'canceled', 'value'=>'canceled', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'canceled'}).appendTo('#edit-form'); submitEditForm(); return true;", 'class' => 'btn canceled']) ?>
        <?= Html::button(' ' . Yii::t('app', 'Update'), ['onclick' => "submitEditForm(); return true;", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::button(' ' . Yii::t('app', 'Delete'), ['name'=>'delete', 'value'=>'delete', 'onclick' => "if (confirm(\"Are you sure?\")) {jQuery('<input>').attr({type:'hidden',name: 'delete'}).appendTo('#edit-form'); submitEditForm(); return true; } else return false;", 'class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>        

	  <?php 
        ActiveForm::end(); 
      ?>
 		</div>
	</div>    
	
<script>
//<![CDATA[    
submitEditForm = function() {
	var _form = $('#edit-form');
	var _data = _form.serialize();
	 $.ajax({
		  url: _form.attr('action'),
		  type: 'post',
		  data: _data,
		  success: function (response) {
				$('#modalEdit').modal('hide');
				onUpdateEvent(_data.indexOf('delete') != -1, response);
		  },
	 });
}
$('#modalEdit').on('submit', 'form', function(event) {
		event.preventDefault();
		 
		 submitEditForm();
		 
		return false;
});
//]]>
</script>

