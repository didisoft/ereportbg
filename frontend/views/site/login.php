<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use yii\authclient\widgets\AuthChoice;	

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= $this->title ?></h2>

<div>                
<?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth']]); ?>
<?php foreach ($authAuthChoice->getClients() as $client) {
        if ($client->getName() == 'google') {
            $client->returnUrl = Yii::$app->params['siteUrl'] . '/site/auth?authclient=google';
        }
        echo $authAuthChoice->clientLink($client, Html::tag('span', $client->getTitle(), ['class' => 'auth-icon ' . $client->getName()]));
      } ?>
<?php AuthChoice::end(); ?>                
</div>			            
<div style="clear: both"></div>
<hr/>

<div class="site-login">
    
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
    
        </div>
    </div>
</div>

<div style="border: 1px solid #dddddd;padding: 10px;">
    <?= Yii::t('app','Lost your password?') ?> <?= Html::a(Yii::t('app','Recover lost password'), ['site/request-password-reset']) ?>
    <br/>
    <?= Yii::t('app','Don\'t have an account?') ?> <?= Html::a(Yii::t('app','Register'), ['site/signup']) ?>
</div>