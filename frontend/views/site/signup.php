<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use yii\authclient\widgets\AuthChoice;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
</div>    

<?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/reg']]); ?>
<?php foreach ($authAuthChoice->getClients() as $client) {
        if ($client->getName() == 'google') {
            $client->returnUrl = Yii::$app->params['siteUrl'] . '/site/reg?authclient=google';
        }
        echo $authAuthChoice->clientLink($client, Html::tag('span', $client->getTitle(), ['class' => 'auth-icon ' . $client->getName()]));
      } ?>
<?php AuthChoice::end(); ?>                

<div class="row">            
            
		<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <hr/>
        <div class="col-lg-5">
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'companyName') ?>
                <?php
                    $countries = ['DE' => Yii::t('app', 'Germany'),
                        'AT' => Yii::t('app', 'Austria'),
                        'CH' => Yii::t('app', 'Switzerland'),
                        'LU' => Yii::t('app', 'Luxemburg'),
                        'BG' => Yii::t('app', 'Bulgaria')]
                ?>
                <?= $form->field($model, 'countryCode')->dropDownList($countries, ['style'=>'width:300px']) ?>            
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
				<?php echo Yii::t('app', 'By registering, you agree to the <a href="{url}">terms of service</a>.',
                            array('url' => $termsUrl));
				?>
                <hr/>
                <?= Html::submitButton(Yii::t('app', 'Register'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>				
        </div>
        <?php ActiveForm::end(); ?>		
</div>

<div style="border: 1px solid #dddddd;padding: 10px; margin-top: 20px;">
    <?= Yii::t('app','Already have an account?') ?> <?= Html::a(Yii::t('app','Login'), ['site/login']) ?>
</div>

