<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use dmstr\widgets\Alert;
 
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
 
 /* @var $this \yii\web\View */
 /* @var $content string */
 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>	
	
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->  
  <link href="<?php echo Yii::$app->request->baseUrl ?>/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo Yii::$app->request->baseUrl ?>/login.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>

    <?php $this->beginBody() ?>    
<nav class="navbar" role="navigation">        
      <div class="nav-wrapper">
          <a id="logo-container" href="#" class="topnav-brand">
                <img src="/wp-content/themes/SaaS-I/images/erinerungsbot.png" alt="<?php echo Html::encode(\Yii::$app->name); ?>" />
          </a>
            <?php
			echo Nav::widget([]); // for bootstrap css
            $menuItems = [];
            //$menuItems[] = ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
                $menuItems[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']];
                $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
				echo Menu::widget([
					'options' => ['id' => "nav-mobile", 'class' => "topnav"],
					'items' => $menuItems
				]);
            ?>
      </div>
      </nav>


    
  <div class="container">
      <nav>
        <div class="nav-wrapper">
        <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb']
            ]) ?>
            </div>
      </nav>
      <div class="section">
      <div class="row">
        <div class="col s12 m12">
           <?= Alert::widget() ?>

    
            <?= $content ?>
        </div>
      </div>
    </div>
  </div>

 <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>