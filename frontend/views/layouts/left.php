<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => Yii::t('app', 'Appointments'), 'icon' => 'fa fa-calendar', 'url' => ['/app/index']],
                    ['label' => Yii::t('app', 'Schedules'), 'icon' => 'fa fa-bookmark-o', 'url' => ['/schedules/index']],
                    ['label' => Yii::t('app', 'Clients'), 'icon' => 'fa fa-user', 'url' => ['/clients/index']],
                    ['label' => Yii::t('app', 'Billing'), 'icon' => 'fa fa-bank', 'url' => ['/billing/index']],
					['label' => Yii::t('app', 'Settings'), 'icon' => 'fa fa-wrench', 'url' => ['/settings/index']],
                    ['label' => Yii::t('app', 'Logout'), 'icon' => 'fa fa-power-off', 'url' => ['site/logout'] ],
                ],
            ]
        ) ?>

    </section>

</aside>
