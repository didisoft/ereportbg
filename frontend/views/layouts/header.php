<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <?= Html::a('<span class="logo-mini">Er</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <?php if (count(\Yii::$app->user->identity->getRequestedContacts()) > 0) { ?>
                            <span class="label label-warning"><?= count(\Yii::$app->user->identity->getRequestedContacts()) ?></span>
                        <?php } ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= count(\Yii::$app->user->identity->getRequestedContacts()) ?> notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <?php foreach (\Yii::$app->user->identity->getRequestedContacts() as $appointments) { ?>
                                <li>
                                    <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(["app/index", "gotoId" => $appointments['AppointmentID']]) ?>">
                                        <h4>                                            
                                            <?php echo $appointments['ClientName'] ?>
                                            <small><i class="fa fa-clock-o"></i> <?php echo (new \DateTime($appointments['StartTime']))->format('d M H::i') ?></small>
                                        </h4>
                                        <p>Phone: <?= $appointments['Phone'] ?></p>
                                        <p>Mobile: <?= $appointments['Mobile'] ?></p>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <!--li class="footer"><a href="#">View all</a></li-->
                    </ul>
                </li>
                                
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->Email ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <p>
                                <?= Yii::$app->user->identity->Email ?>
                                <small></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    Yii::t('app', 'Settings'),
                                    ['/settings/index'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('app', 'Logout'),
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
