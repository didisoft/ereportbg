<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <li class="header">
    <?= Html::beginForm(['site/language']) ?>
    <?= Html::dropDownList('language', Yii::$app->language, ['en' => 'English', 'de' => 'Deutsch']) ?>
    <?= Html::submitButton('Change', ['class' => 'btn btn-default btn-flat']) ?>
    <?= Html::endForm() ?>					
    </li>
    
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong><?= \Yii::$app->params['siteName'] ?> &copy; 2016-<?= date('Y') ?>
</footer>

<div class='control-sidebar-bg'></div>