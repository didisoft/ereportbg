<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <meta charset="<?= Yii::$app->charset ?>"/>
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->  
  <link href="<?php echo Yii::$app->request->baseUrl ?>/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>

    <?php $this->beginBody() ?>
<nav class="navbar" role="navigation">        
      <div class="nav-wrapper"><a id="logo-container" href="#" class="topnav-brand"><?php echo Html::encode(\Yii::$app->name); ?></a>
            <?php
            $menuItems = [];
            //$menuItems[] = ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']];
                $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
            } else {
				$menuItems[] = ['label' => Yii::t('app', 'Appointments'), 'url' => ['/appointments/index']];
				$menuItems[] = ['label' => Yii::t('app', 'Schedules'), 'url' => ['/schedules/index']];
				$menuItems[] = ['label' => Yii::t('app', 'Clients'), 'url' => ['/clients/index']];
                $menuItems[] = ['label' => Yii::t('app', 'Subscription'), 'url' => ['/billing/index']];
				$menuItems[] = ['label' => Yii::t('app', 'Settings'), 'url' => ['/settings/index']];
                $menuItems[] = [
                    'label' => Yii::t('app', 'Logout') .'(' . Yii::$app->user->identity->Email . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }            
                    echo Menu::widget([
                        'options' => ['id' => "nav-mobile", 'class' => "topnav"],
                        'items' => $menuItems
                    ]);
            ?>
      </div>
      </nav>


    
  <div class="container">
      <nav>
        <div class="nav-wrapper">
        <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb']
            ]) ?>
            </div>
      </nav>
      <div class="section">
      <div class="row">
        <div class="col s12 m12">
    
            <?= $content ?>
        </div>
      </div>
    </div>
  </div>

 <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>