<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\popover\PopoverX;

$this->title = Yii::t('app', 'Phone calls');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form ActiveForm */

\yii\jui\JuiAsset::register($this);
\yii\bootstrap\BootstrapAsset::register($this);
?>


<div class="box box-primary">
            <!-- form start -->
        <div class="box-header">
            <?= Yii::t('app', 'You can customize what we tell your clients, via Phone, SMS, or Email. Our computers automatically insert per-appointment information (like Appointment Time) into your messages.') ?>
        </div>

    <?php $form = ActiveForm::begin(); ?>
              <div class="box-body">

    <div role="tabpanel" class="tab-pane active" id="home">
		<div id="tabs-1">
			<?= $form->field($model, 'PhoneText')->textarea() ?>
            <b><?= Yii::t('app', 'Insert') ?> </b>
            <a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonetext').insertAtCaret('<?= Placeholder_Company ?>')"><?= Placeholder_Company ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonetext').insertAtCaret('<?= Placeholder_Client ?>')"><?= Placeholder_Client ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonetext').insertAtCaret('<?= Placeholder_AppointmentTime ?>')"><?= Placeholder_AppointmentTime ?></a>
        <?php        
        $hintText = '<b>'. Placeholder_Company . '</b> ';
                if (Yii::$app->user->identity->getCustomer()->CompanyName) {
                    $hintText .= Yii::t('app', 'will be automatically replaced with');
                    $hintText .= '&nbsp;';
                    $hintText .= Yii::$app->user->identity->getCustomer()->CompanyName;
                } else {
                    $hintText .= Yii::t('app', 'Company Name must be filled');
                    $hintText .= '&nbsp;';
                    $hintText .= Html::a(Yii::t('app', 'Edit'), ['settings/company'], ['class' => 'btn-sm btn-success']);
                }
            
        $hintText .= '<br/>';
        $hintText .= '<br/>';
        $hintText .= '<b>'. Placeholder_Client . '</b> ';
        $hintText .= Yii::t('app', 'will be automatically replaced with');
        $hintText .= '&nbsp;';
        $hintText .= Yii::t('app', 'the name of the client, for example: Peter Müller');
        $hintText .= '<br/>';
        $hintText .= '<br/>';
        $hintText .= '<b>'. Placeholder_AppointmentTime . '</b> ';
        $hintText .= Yii::t('app', 'will be automatically replaced with');
        $hintText .= '&nbsp;';
        $hintText .= Yii::t('app', 'the time of the appointment, for example: June 22 at 10:30'); 
                
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_DEFAULT,
				'placement' => PopoverX::ALIGN_BOTTOM,
				'content' => $hintText,
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
		?>	                    
		</div>
	</div>		

    <hr/>                                
    <div role="tabpanel" class="tab-pane" id="profile">	
		<div id="tabs-2">
			<?= $form->field($model, 'PhoneCallbackText')->textarea() ?>
            <b><?= Yii::t('app', 'Insert') ?> </b>
            <a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecallbacktext').insertAtCaret('<?= Placeholder_Company ?>')"><?= Placeholder_Company ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecallbacktext').insertAtCaret('<?= Placeholder_Client ?>')"><?= Placeholder_Client ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecallbacktext').insertAtCaret('<?= Placeholder_AppointmentTime ?>')"><?= Placeholder_AppointmentTime ?></a>
        <?php        
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_DEFAULT,
				'placement' => PopoverX::ALIGN_BOTTOM,
				'content' => $hintText,
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
		?>	                    
		</div>
	</div>

    <hr/>                                
    <div role="tabpanel" class="tab-pane" id="profile">	
		<div id="tabs-2">
			<?= $form->field($model, 'PhoneConfirmText')->textarea() ?>
            <b><?= Yii::t('app', 'Insert') ?> </b>
            <a href="#" class="btn btn-primary" onclick="jQuery('#customers-phoneconfirmtext').insertAtCaret('<?= Placeholder_Company ?>')"><?= Placeholder_Company ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phoneconfirmtext').insertAtCaret('<?= Placeholder_Client ?>')"><?= Placeholder_Client ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phoneconfirmtext').insertAtCaret('<?= Placeholder_AppointmentTime ?>')"><?= Placeholder_AppointmentTime ?></a>
            <?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_DEFAULT,
				'placement' => PopoverX::ALIGN_BOTTOM,
				'content' => $hintText,
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
            ?>	                                
		</div>
	</div>

    <hr/>                                
    <div role="tabpanel" class="tab-pane" id="profile">	
		<div id="tabs-2">
			<?= $form->field($model, 'PhoneCancelText')->textarea() ?>
            <b><?= Yii::t('app', 'Insert') ?> </b>
            <a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecanceltext').insertAtCaret('<?= Placeholder_Company ?>')"><?= Placeholder_Company ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecanceltext').insertAtCaret('<?= Placeholder_Client ?>')"><?= Placeholder_Client ?></a>
			<a href="#" class="btn btn-primary" onclick="jQuery('#customers-phonecanceltext').insertAtCaret('<?= Placeholder_AppointmentTime ?>')"><?= Placeholder_AppointmentTime ?></a>
            <?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_DEFAULT,
				'placement' => PopoverX::ALIGN_BOTTOM,
				'content' => $hintText,
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
            ?>	                                
		</div>
	</div>
  
              </div>              
                  
        <div class="box-footer">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
                  
                  
    <?php ActiveForm::end(); ?>
    
<?php
$js = <<<EOT
 $('#tabs').tabs();

  jQuery.fn.extend({
insertAtCaret: function(myValue){
  return this.each(function(i) {
    if (document.selection) {
      //For browsers like Internet Explorer
      this.focus();
      var sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    else if (this.selectionStart || this.selectionStart == '0') {
      //For browsers like Firefox and Webkit based
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  });
}
});
EOT;
$this->registerJs($js, $this::POS_READY);
?>

</div><!-- settings-notificationdata -->
