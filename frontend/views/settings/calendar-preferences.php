<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

$this->title = Yii::t('app', 'Calendar Settings');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

$hours = 		[0 => '00:00',
		1 => '01:00',
		2 => '02:00',
		3 => '03:00',
		4 => '04:00',
		5 => '05:00',
		6 => '06:00',
		7 => '07:00',
		8 => '08:00',
		9 => '09:00',
		10 => '10:00',
		11 => '11:00',
		12 => '12:00',
		13 => '13:00',
		14 => '14:00',
		15 => '15:00',
		16 => '16:00',
		17 => '17:00',
		18 => '18:00',
		19 => '19:00',
		20 => '20:00',
		21 => '21:00',
		22 => '22:00',
		23 => '23:00'];
/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="box box-primary">
            <!-- form start -->

    <?php $form = ActiveForm::begin(); ?>
              <div class="box-body">

	<?= $form->field($model, 'StartHour')->dropDownList($hours, 
		['style'=>'width:100px', 'class'=>'theme']);
	?>

	<?= $form->field($model, 'EndHour')->dropDownList($hours, 
		['style'=>'width:100px', 'class'=>'theme']);
	?>
    
	<?= $form->field($model, 'MinLength')->dropDownList(
		[5 => '5 minutes',
        10 => '10 minutes',    
		15 => '15 minutes',
        20 => '20 minutes',        
		30 => '30 minutes',
		60 => '60 minutes'], 
		['style'=>'width:150px', 'class'=>'theme']);
	?>
    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    </div>            
    <?php ActiveForm::end(); ?>
            <div class="box-footer">
                <?= Yii::t('app', "You can configure what days and times are available for the Calendar") ?>
            </div>
</div><!-- settings -->
