<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\authclient\widgets\AuthChoice;	

$this->title = Yii::t('app','Settings');
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>

<div class="row">    
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-user-o"></i> <?= Yii::t('app', 'Account') ?></h3>
          </div><!-- /.box-header -->
          <div class="box-body">
              <p>
              <?= Html::a(Yii::t('app', 'Edit'), ['settings/company' ], ['class' => 'btn btn-info']) ?>
              <span style="font-size: 1.4em; font-weight: bold;"><?= Yii::t('app', 'Company Name') ?></span>
              </p>
              <p>
              <?= Html::a(Yii::t('app', 'Edit'), ['settings/settings' ], ['class' => 'btn btn-info']) ?>
              <span style="font-size: 1.4em; font-weight: bold;"><?= Yii::t('app', 'Account') ?></span>
              </p>
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/password' ], ['class' => 'btn btn-info']) ?>
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Password') ?></span>                
              </p>  
              <p>
                <?php if ($model->IsAdmin) { ?>
                    <?php echo Html::a(Yii::t('app', 'Edit'), ['logins/index' ], ['class' => 'btn btn-info']) ?>
                    <span style="font-size: 1.4em; font-weight: bold;"><?= Yii::t('app', 'Additional Users') ?></span>
                <?php } ?>        
              </p>  
          </div><!-- /.box-body -->
          <div class="box-footer">
              <div class="info-box-text" style="float: left">  
                    <?= Yii::t('app', 'Connect my account with') ?>&nbsp;  
              </div>
            <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['settings/connect']]); ?>
            <?php foreach ($authAuthChoice->getClients() as $client) {
                    if ($client->getName() == 'google') {
                        $client->returnUrl = Yii::$app->params['siteUrl'] . '/settings/connect?authclient=google';
                    }
                    echo $authAuthChoice->clientLink($client, Html::tag('span', $client->getTitle(), ['class' => 'auth-icon ' . $client->getName()]));
                  } ?>
            <?php AuthChoice::end(); ?>                
          </div><!-- box-footer -->
        </div><!-- /.box -->
        <br/>
    </div>
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bell"></i> <?= Yii::t('app', 'Notifications') ?></h3>
          </div><!-- /.box-header -->
          
          <div class="box-body">
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/notification-email' ], ['class' => 'btn btn-info']) ?>                
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Emails text') ?></span>
              </p>
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/notification-sms' ], ['class' => 'btn btn-info']) ?>                
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'SMS text') ?></span>
              </p>
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/notification-phone' ], ['class' => 'btn btn-info']) ?>                        
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Phone calls') ?></span>
              </p>  
              <hr/>
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/notification-preferences' ], ['class' => 'btn btn-info']) ?>                        
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Notification Timing') ?></span>
              </p>  
          </div><!-- /.box-body -->
          <div class="box-footer">
              <?= Yii::t('app', 'You can customize what we tell your clients, via Phone, SMS, or Email. Our computers automatically insert per-appointment information (like Appointment Time) into your messages.') ?>
          </div><!-- box-footer -->
        </div><!-- /.box -->
        <br/>
    </div>
</div>

<div class="row">
    <div class="col-md-6">        
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-calendar"></i> <?= Yii::t('app', 'Calendar') ?></h3>
          </div>  
            <div class="box-body">
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/calendar-preferences' ], ['class' => 'btn btn-info']) ?>                        
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Calendar Settings') ?></span>
              </p>                  
            </div>            
        </div>
    </div>
</div>

<div class="row">    
    <div class="col-md-12">
        &nbsp;
    </div>
</div>


<div class="row">
    <div class="col-md-4">        
    </div>
    <div class="col-md-8">
    </div>
</div>

<div class="row">    
    <div class="col-md-12">
        &nbsp;
    </div>
</div>




