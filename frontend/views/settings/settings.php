<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

$this->title = Yii::t('app', 'Personal data');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="box box-primary">
            <!-- form start -->

    <?php $form = ActiveForm::begin(); ?>
              <div class="box-body">

        <?= $form->field($model, 'FirstName')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>
        <?= $form->field($model, 'LastName')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>
		<?= $form->field($model, 'Email')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>
    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div><!-- settings -->
