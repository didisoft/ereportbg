<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

$this->title = Yii::t('app', 'Notification Timing');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>

<div class="row">    
    <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Appointments') ?></h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'IntervalMinutesEmail')->dropDownList(
                    \common\helpers\NotificationHelper::notificationValues(),
                    ['class'=>'theme']);
                ?>

                <?= $form->field($model, 'IntervalMinutesSMS')->dropDownList(
                    \common\helpers\NotificationHelper::notificationValues(),
                    ['class'=>'theme']);
                ?>

                <?= $form->field($model, 'IntervalMinutesPhone')->dropDownList(
                    \common\helpers\NotificationHelper::notificationValues(),
                    ['class'=>'theme']);
                ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
                </div>
            <?php ActiveForm::end(); ?>              
          </div>
          
          <div class="box-footer">
              <?= Yii::t('app', 'Our system will automatically reminds customers in advance of their appointment. You can control when you want this to happen.') ?>
              
              <br/>
              <br/>
              <?= Yii::t('app', 'Important facts to keep in mind') ?>
              <ul>
                  <li>
                      <?= Yii::t('app', 'If a user confirms, cancels, or requests you to contact them about an appointment, we stop trying to contact them about it.') ?>
                  </li>
                  <li>
                      <?= Yii::t('app', 'We never deliver a reminder after the scheduled appointment start time.') ?>
                  </li>
                  <li>
                      <?= Yii::t('app', 'Reminders generally go out a few minutes after the time scheduled') ?>
                  </li>
              </ul>
          </div>          
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Requested Contact') ?></h3>
          </div><!-- /.box-header -->
          
          <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'NotificationPrefs')->dropDownList(
                \common\helpers\NotificationHelper::selfNotificationValues(),
                ['class'=>'theme']);
            ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
                </div>
            <?php ActiveForm::end(); ?>              
          </div>
          
          <div class="box-footer">
              <?= Yii::t('app', 'How would you like to be notified when your customers requested contact for an appointment?') ?>
          </div>
        </div>
    </div>
</div>
