<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

    <?php $form = ActiveForm::begin(); ?>
              <div class="box-body">

		<div class="form-group">
		<label style="width: 200px;" for="old_password"><?= Yii::t('app', 'Current password') ?></label>
        <?= Html::passwordInput('old_password') ?>
		</div>
		
		<div class="form-group">
		<label style="width: 200px;" for="new_password"><?= Yii::t('app', 'New password') ?></label>
		<?= Html::passwordInput('new_password') ?>
		</div>
		
		<div class="form-group">
		<label style="width: 200px;" for="repeat_password"><?= Yii::t('app', 'Repeat new password') ?></label>
		<?= Html::passwordInput('repeat_password') ?>
		</div>
    
        <div class="form-group">		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    </div>             
    <?php ActiveForm::end(); ?>

                  
</div>
