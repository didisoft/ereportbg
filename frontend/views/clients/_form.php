<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

<?php if ($model->isNewRecord) { 
	$model->SendEmail = 1;
	$model->SendPhone = 1;
	$model->SendMobile = 1;
}
?>
	
    <?= $form->field($model, 'Name')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>

    <?= $form->field($model, 'Email')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>

    <?= $form->field($model, 'Phone')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>

    <?= $form->field($model, 'Mobile')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>

    
	<?php
    //echo $form->field($model, 'SendEmail')->widget(CheckboxX::classname(), ['autoLabel'=>true])->label(false); 
    ?>

	<?php
    // echo $form->field($model, 'SendPhone')->widget(CheckboxX::classname(), ['autoLabel'=>true])->label(false); 
    ?>

	<?php
    // echo $form->field($model, 'SendMobile')->widget(CheckboxX::classname(), ['autoLabel'=>true])->label(false); 
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['clients/index']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
