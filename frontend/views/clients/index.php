<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        
        <?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_DEFAULT,
				'placement' => PopoverX::ALIGN_BOTTOM,
				'content' => nl2br(Yii::t('app', "To save you time typing, we can import a list of clients from many popular software systems.
\nIf you would like us to handle the upload for you, please send us your list of clients. The best format to send them in is CSV or Microsoft Excel. If you aren't sure how to get this, check options for exporting in the software you presently use, or ask us for help.
\nThere is no charge for this service.")),
				'toggleButton' => ['label'=>Yii::t('app', 'Import clients'), 'class'=>'btn btn-info'],
			]);	
		?>	        
    </p>

<?php
// Filter as you type
/*
$this->registerJs('$("body").on("keyup.yiiGridView", "#clients-grid .filters input", function(){
    $("#clients-grid").yiiGridView("applyFilter");
})', \yii\web\View::POS_READY);    
 */
?>
    
<div class="box box-primary">
            <!-- form start -->

              <div class="box-body">    
    <?php //\yii\widgets\Pjax::begin(['id' => 'datatable-user', 'timeout' => false, 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'id' => 'clients-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Name',
            'Email:email',
            'Phone',
            'Mobile',
            // 'SendEmail:email',
            // 'SendPhone',
            // 'SendMobile',

            ['class' => 'yii\grid\ActionColumn', 
             'template' => '{update}&nbsp;&nbsp;{delete}',
                'buttons'=>[
                              'update' => function ($url, $model) {     
                                return Html::a(Yii::t('yii', 'Update'), $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-default fa fa-edit'
                                ]);                                
                              },
                                'delete' => function ($url, $model) {     
                                return Html::a(Yii::t('yii', 'Delete'), $url, [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'class' => 'btn btn-default fa fa-trash'
                                  ]);                                
                                }
                ],                                          
           ],
        ],
    ]); ?>
    <?php //\yii\widgets\Pjax::end(); ?>              
  </div>
</div>
