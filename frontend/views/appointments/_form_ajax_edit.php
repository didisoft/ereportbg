<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = 
            ActiveForm::begin([
                   'options' => [
                    'id' => 'edit-form',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form', 
                ]]);    
	?>

        <?= $form->errorSummary($model); ?>        

        <input type="hidden" id="ClientID" name="Appointments[ClientID]" value="0"/>
        <?= $form->field($model, 'ScheduleID', ['template' => '{input}'])->hiddenInput() ?>
        <?= $form->field($model, 'ClientName',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'                        
                    ],
                    'options' => ['class'=>'ui.input']
                ]
                )->textInput() ?>              
        <?= $form->field($model, 'Phone',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Mobile',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Email',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                

        <?= $form->field($model, 'StartTime', ['template' => '{input}'])->hiddenInput() ?>                
        <?= $form->field($model, 'EndTime', ['template' => '{input}'])->hiddenInput() ?>                
        
        
        <p style="text-align: ceter">
            Mark as: 
            <?= Html::submitButton('Confirmed', ['name'=>'delete', 'value'=>'delete', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'confirmed'}).appendTo('#edit-form'); return true;", 'class' => 'btn confirmed']) ?>
            <?= Html::submitButton('Canceled', ['name'=>'canceled', 'value'=>'canceled', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'canceled'}).appendTo('#edit-form'); return true;", 'class' => 'btn canceled']) ?>
            <?= Html::submitButton('Showed', ['name'=>'showed', 'value'=>'showed', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'showed'}).appendTo('#edit-form'); return true;", 'class' => 'btn showed']) ?>
            <?= Html::submitButton('No Showed', ['name'=>'no_showed', 'value'=>'no_showed', 'onclick' => "jQuery('<input>').attr({type:'hidden',name: 'no_showed'}).appendTo('#edit-form'); return true;", 'class' => 'btn no_showed']) ?>
        </p>

        <p style="text-align: ceter">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::submitButton('Delete', ['name'=>'delete', 'value'=>'delete', 'onclick' => "if (confirm(\"Are you sure?\")) {jQuery('<input>').attr({type:'hidden',name: 'delete'}).appendTo('#edit-form'); return true;} return false;", 'class' => 'btn btn-success']) ?>
        <a href="#!" rel="modal:close" class="modal-close waves-effect waves-blue btn-flat">Cancel</a>
        </p>

    <?php 
        ActiveForm::end(); 
    ?>
    <script>
    //<![CDATA[    
function submitUpdate(ev) {
     ev.preventDefault();

     var form = jQuery(this);     
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }    
     form.validate();
     alert(form.valid());
     if (!form.valid()) {
         return false;
     }
     
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
                jQuery('.modal-close').click();
                jQuery('#calendar').weekCalendar('refresh');
          },
     });
};     

jQuery( "form#edit-form #appointments-clientname" ).autocomplete({
  source : '<?= Yii::$app->urlManager->createAbsoluteUrl(["clients/autocomplete2"]) ?>',      
  appendTo: "#modalEdit",      
  delay : 0,      
  select: function( event, ui ) {
    jQuery('#edit-form #ClientID').val(ui.item.id);
    jQuery('#edit-form #appointments-phone').val(ui.item.phone);
    jQuery('#edit-form #appointments-mobile').val(ui.item.mobile);
    jQuery('#edit-form #appointments-email').val(ui.item.email);
  }
});

jQuery('form#edit-form').validate({
'rules': {
    'Appointments[Mobile]':{'required':false, 'remote':'<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/check-phone"]) ?>'},
    'Appointments[Phone]':{'required':false, 'remote':'<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/check-phone"]) ?>'}    
},
'messages': {
    'Appointments[Mobile]': { 'remote': '<?= Yii::t('app', 'The specified phone number does not seem to be valid') ?>' },
    'Appointments[Phone]': { 'remote': '<?= Yii::t('app', 'The specified phone number does not seem to be valid')?>' },
},
submitHandler : submitUpdate
});
//]]>
        </script>

