<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MentionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mentions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'MentionID') ?>

    <?= $form->field($model, 'CustomerID') ?>

    <?= $form->field($model, 'WordID') ?>

    <?= $form->field($model, 'SourceID') ?>

    <?= $form->field($model, 'JobID') ?>

    <?php // echo $form->field($model, 'Title') ?>

    <?php // echo $form->field($model, 'Link') ?>

    <?php // echo $form->field($model, 'Snippet') ?>

    <?php // echo $form->field($model, 'MentionDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
