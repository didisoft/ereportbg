<?php
////////////////////////////////////////////////////////////////////////////
// Prints weekCalendar javascript
//
// Options:
// https://github.com/themouette/jquery-week-calendar/wiki/Script-options
///////////////////////////////////////////////////////////////////////////
?>
var g_urlEventNew = "<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/create"]) ?>";
var g_urlEventEdit = "<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/update?id="]) ?>";

function formatDate(d) {
        return d.getFullYear() + '-' + appendZero(d.getMonth()+1) + '-' + appendZero(d.getDate()) + ' ' + appendZero(d.getHours()) + ':' + appendZero(d.getMinutes()) + ':00';
}

function appendZero(x) {
        if ((""+x).length == 1) 
            x = "0"+x;
        return x;
}

function show_modal(request) {
	jQuery('#modalOpenEdit').click();
	jQuery('#modalContent').html(request);	
}

function onEventNew(calEvent, event, FreeBusyManager, calendar) {
    jQuery('#modalOpenNew').click();
	jQuery('#create-form').prop('action', '<?php echo Yii::$app->request->baseUrl ?>/appointments/create');
    jQuery('#create-form').validate().resetForm();
	jQuery('.help-block-error').empty();
    jQuery('#appointments-clientname').val('');
	jQuery('#appointments-phone').val('');
	jQuery('#appointments-mobile').val('');
	jQuery('#appointments-email').val('');
	jQuery('#appointments-starttime').val(formatDate(calEvent.start));
	jQuery('#appointments-endtime').val(formatDate(calEvent.end));
    jQuery('#appointments-scheduleid').val(calEvent.userId);
}

jQuery('#calendar').weekCalendar({
      data: '<?php echo Yii::$app->request->baseUrl ?>/appointments/load-calendar-data',
      switchDisplay: {'Work week': 5, 'Full week': 7},
      timeslotsPerHour: 4,
      businessHours: {start: <?= $schedule->StartHour ?>, end: <?= $schedule->EndHour ?>, limitDisplay: true},
      users: [{"id": <?= $schedule->ScheduleID ?>, "name": "<?= $schedule->Name ?>"}],
      getUserId: function(user, index, calendar) {
            return user.id;
      },
      getUserName: function(user, index, calendar) {
            return user.name;
      },		      
      height: function($calendar) {
        //return $(window).height() - $('.navbar').outerHeight(true) - $('#calendar-selection').height();
        return $(window).height();
    },
      eventRender: function(calEvent, $event) {
        if (calEvent.end.getTime() < new Date().getTime()) {
          $event.css('backgroundColor', '#aaa');
          $event.find('.time').css({
            //backgroundColor: '#999',
            border:'1px solid #888'
          });
        } else if (calEvent.status) {
          $event.addClass(calEvent.status);
        }           
      },
      eventNew: function(calEvent, $event) {
        onEventNew(calEvent, $event);
      },
      eventDrop: function(calEvent, $event) {
		jQuery.ajax({
				  complete:function(request){ 
						$('#calendar').weekCalendar('refresh');
				  }, 
				  data:'Appointments[StartTime]=' + formatDate(calEvent.start) + '&Appointments[EndTime]=' + formatDate(calEvent.end) + '&Appointments[ScheduleID]=' + 1, 
				  type:'post', 
				  url: '<?php echo Yii::$app->request->baseUrl ?>/appointments/update?id='+calEvent.id
			});
      },
      eventResize: function(calEvent, $event) {
		jQuery.ajax({
				  complete:function(request){ 
						$('#calendar').weekCalendar('refresh');
				  }, 
				  data:'Appointments[StartTime]=' + formatDate(calEvent.start) + '&Appointments[EndTime]=' + formatDate(calEvent.end) + '&Appointments[ScheduleID]=' + 1, 
				  type:'post', 
				  url: '<?php echo Yii::$app->request->baseUrl ?>/appointments/update?id='+calEvent.id
			});		
      },
      eventClick: function(calEvent, $event) {
        jQuery.ajax({data:'id=' + calEvent.id, success:function(request){ show_modal(request); }, type:'post', url:g_urlEventEdit+calEvent.id});        
      },
      
        useShortDayNames: false,
        // I18N
        firstDayOfWeek: $.datepicker.regional['<?= Yii::$app->language ?>'].firstDay,
        shortDays: $.datepicker.regional['<?= Yii::$app->language ?>'].dayNamesShort,
        longDays: $.datepicker.regional['<?= Yii::$app->language ?>'].dayNames,
        shortMonths: $.datepicker.regional['<?= Yii::$app->language ?>'].monthNamesShort,
        longMonths: $.datepicker.regional['<?= Yii::$app->language ?>'].monthNames,
        dateFormat: 'd F y'      
    });

