<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mentions */

$this->title = 'Create Mentions';
$this->params['breadcrumbs'][] = ['label' => 'Mentions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mentions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
