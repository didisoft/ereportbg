<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = 
            ActiveForm::begin([
                   'options' => [
                    'id' => 'create-form',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form', 
                    'enableAjaxValidation' => true,	
                ]]);    
	?>

        <?= $form->errorSummary($model); ?>        

        <input type="hidden" id="ClientID" name="Appointments[ClientID]" value="0"/>
        <?= $form->field($model, 'ScheduleID', ['template' => '{input}'])->hiddenInput() ?>
        <?= $form->field($model, 'ClientName',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'                        
                    ],
                    'options' => ['class'=>'ui.input']
                ]
                )->textInput() ?>              
        <?= $form->field($model, 'Phone',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Mobile',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                
        <?= $form->field($model, 'Email',
                [
                    'inputOptions'=>[
                        'autocomplete'=>'off'
                    ],
                ]
                )->textInput() ?>                

        <?= $form->field($model, 'StartTime', ['template' => '{input}'])->hiddenInput() ?>                
        <?= $form->field($model, 'EndTime', ['template' => '{input}'])->hiddenInput() ?>                
        
        
        <p style="text-align: ceter">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a href="#!" rel="modal:close" class="modal-close waves-effect waves-blue btn-flat">Cancel</a>
	</p>
    <?php 
        ActiveForm::end(); 
    ?>

