<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MentionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Appointments');
?>

<a href="#modalNew" rel="modal:open" id="modalOpenNew"></a>
<div id="modalNew" style="display:none;">
   <?php
echo \Yii::$app->view->render('@frontend/views/appointments/_form_ajax', ['model'=> new \common\models\Appointments()]);
    ?>
</div>    
<a href="#modalEdit" rel="modal:open" id="modalOpenEdit"></a>
<div id="modalEdit" style="display:none;">
<div id="modalContent" class="modal-content">    
</div>
</div>
<?php 


$js = '';
$js .= 'var g_urlCalendar = "'. Yii::$app->urlManager->createAbsoluteUrl(["appointments/load-calendar?ids="]) . '";';
$js .= 'var g_urlClients = "'. Yii::$app->urlManager->createAbsoluteUrl(["clients/autocomplete2"]) . '";';
$js .= <<<EOT
jQuery('#data_source').change(function() {
  jQuery('#calendar').weekCalendar('refresh');

  var scheduleId = jQuery('#data_source').val();
  jQuery.ajax({
    url: g_urlCalendar+scheduleId,
    dataType: "script"});                        
});

        
///////////////////////
//
// Event form
//
///////////////////////        
  
function submitCreate() {
     var form = jQuery(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     if (!form.valid()) {
         return false;
     }
         
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
                jQuery('.modal-close').click();
                jQuery('#calendar').weekCalendar('refresh');
          },
     });
     return false;
};

jQuery( "#appointments-clientname" ).autocomplete({
  source : g_urlClients,      
  appendTo: "#modalNew",      
  delay : 0,      
  select: function( event, ui ) {
    jQuery('#ClientID').val(ui.item.id);
    jQuery('#appointments-phone').val(ui.item.phone);
    jQuery('#appointments-mobile').val(ui.item.mobile);
    jQuery('#appointments-email').val(ui.item.email);
  }
});
    
$( "#modalNew" ).on( "modal:close", function() {
  $('#calendar').weekCalendar('refresh');
});        

setTimeout(function() {        
jQuery.ajax({
  url: g_urlCalendar,
  dataType: "script"
 });                
}, 100);        
        
EOT;

$js .= 
   "$('form#create-form').validate({
'rules': {
    'Appointments[Email]' : {'required' : true,'email' :true},
    'Appointments[Mobile]':{'required':false, 'remote':'".Yii::$app->urlManager->createAbsoluteUrl(["appointments/check-phone"])."'},
    'Appointments[Phone]':{'required':false, 'remote':'".Yii::$app->urlManager->createAbsoluteUrl(["appointments/check-phone"])."'}    
},
'messages': {
    'Appointments[Email]': {
      'required': 'We need your email address to contact you',
      'email': 'Your email address must be in the format of name@domain.com'
    },
    'Appointments[Mobile]': { 'remote': '".Yii::t('app', 'The specified phone number does not seem to be valid')."' },
    'Appointments[Phone]': { 'remote': '".Yii::t('app', 'The specified phone number does not seem to be valid')."' },
},
submitHandler : submitCreate
});";

?>

  <div id="message" class="ui-corner-all"></div>

<div id="calendar-selection" class="ui-state-highlight ui-corner-all">
    <strong>Schedule: </strong>
    <select id="data_source" class="browser-default" style="width:150px">
        <?php foreach($schedules as $schedule) { ?>
        <option value="<?= $schedule->ScheduleID ?>"><?= $schedule->Name ?></option>
        <?php } ?>
    </select>
    
<div id="color_codes">

<table>
    <tr>
        <td>    
    <strong><a href="#" onclick="$('#color_codes').hide(); $('#color_codes_min').show(); return false">Color Codes -</a></strong>
        </td>
        <td>    
        <td>
<div class="name">
Scheduled
</div>
        </td>
        <td>    
<div class="color-swatch scheduled">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="scheduled_hide_link" onclick="$('.wc-cal-event.scheduled').hide(); $(this).hide(); $('#scheduled_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="scheduled_show_link" onclick="$('.wc-cal-event.scheduled').show(); $(this).hide(); $('#scheduled_hide_link').show();; return false;">Show</a>
        </td>
        <td>    

<div class="name">
Confirmed
</div>
        </td>
        <td>    
<div class="color-swatch confirmed">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="confirmed_hide_link" onclick="$('.wc-cal-event.confirmed').hide(); $(this).hide(); $('#confirmed_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="confirmed_show_link" onclick="$('.wc-cal-event.confirmed').show(); $(this).hide(); $('#confirmed_hide_link').show();; return false;">Show</a>
    </td>
    <td>
<div class="name">
Showed
</div>
        </td>
        <td>    
<div class="color-swatch showed">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="showed_hide_link" onclick="$('.wc-cal-event.showed').hide(); $(this).hide(); $('#showed_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="showed_show_link" onclick="$('.wc-cal-event.showed').show(); $(this).hide(); $('#showed_hide_link').show();; return false;">Show</a>
        </td>
        <td>    

<div class="name">
Notified
</div>
        </td>
        <td>    
<div class="color-swatch notified">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="notified_hide_link" onclick="$('.wc-cal-event.notified').hide(); $(this).hide(); $('#notified_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="notified_show_link" onclick="$('.wc-cal-event.notified').show(); $(this).hide(); $('#notified_hide_link').show();; return false;">Show</a>
        </td>
        <td>    

<div class="name">
Requested Contact
</div>
        </td>
        <td>    
<div class="color-swatch requested_contact">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="requested_contact_hide_link" onclick="$('.wc-cal-event.requested_contact').hide(); $(this).hide(); $('#requested_contact_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="requested_contact_show_link" onclick="$('.wc-cal-event.requested_contact').show(); $(this).hide(); $('#requested_contact_hide_link').show();; return false;">Show</a>
        </td>
        <td>    

<div class="name">
Not Notified
</div>
        </td>
        <td>    
<div class="color-swatch not_notified">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="not_notified_hide_link" onclick="$('.wc-cal-event.not_notified').hide(); $(this).hide(); $('#not_notified_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="not_notified_show_link" onclick="$('.wc-cal-event.not_notified').show(); $(this).hide(); $('#not_notified_hide_link').show();; return false;">Show</a>
        </td>
        <td>    

<div class="name">
Canceled
</div>
        </td>
        <td>    
<div class="canceled color-swatch">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="canceled_hide_link" onclick="$('.wc-cal-event.canceled').hide(); $(this).hide(); $('#canceled_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="canceled_show_link" onclick="$('.wc-cal-event.canceled').show(); $(this).hide(); $('#canceled_hide_link').show();; return false;">Show</a>

        </td>
        <td>    
<div class="name">
No Showed
</div>
        </td>
        <td>    
<div class="color-swatch no_showed">&nbsp;</div>
        </td>
        <td>    
<a href="#" id="no_showed_hide_link" onclick="$('.wc-cal-event.no_showed').hide(); $(this).hide(); $('#no_showed_show_link').show();; return false;">Hide</a>
<a class="hidden" href="#" id="no_showed_show_link" onclick="$('.wc-cal-event.no_showed').show(); $(this).hide(); $('#no_showed_hide_link').show();; return false;">Show</a>

        </td>
    </tr>
</table>
</div>
<div id="color_codes_min" style="display: none">
<table>
    <tr>
        <td>    
    <strong><a href="#" onclick="$('#color_codes').show(); $('#color_codes_min').hide(); return false">Color Codes +</a></strong>
        </td>
    </tr>
</table>
 </div>    

  </div>    
  
<div class="main-content">    
  <div id="calendar"></div>
</div>
  
<?php 
$this->registerJs($js, $this::POS_READY);
?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/js/jquery-ui-1.8.11.custom.css'); ?>  
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/js/calendar/jquery.weekcalendar.css'); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/js/calendar/skins/default.css'); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/js/calendar/skins/gcalendar.css'); ?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/calendar/date.js',
		['depends' => [\yii\jui\JuiAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/calendar/datepicker-de.js',
		['depends' => [\yii\jui\JuiAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/calendar/jquery.weekcalendar.js',
		['depends' => [\yii\jui\JuiAsset::className()]]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/scripts.js'); ?>

