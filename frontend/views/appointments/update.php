<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */

$this->title = 'Update Mentions: ' . ' ' . $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Mentions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'id' => $model->MentionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mentions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
