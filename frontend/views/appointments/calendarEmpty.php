var g_urlEventNew = "<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/create"]) ?>";
var g_urlEventEdit = "<?= Yii::$app->urlManager->createAbsoluteUrl(["appointments/update?id="]) ?>";

function formatDate(d) {
        return d.getFullYear() + '-' + appendZero(d.getMonth()+1) + '-' + appendZero(d.getDate()) + ' ' + appendZero(d.getHours()) + ':' + appendZero(d.getMinutes()) + ':00';
}



function appendZero(x) {
        if ((""+x).length == 1) 
            x = "0"+x;
        return x;
}



function getData(start, end, callback) {
   if (start < g_start || g_end < start) {
        jQuery.ajax({
            url: 'http://localhost:3000/erinnerungsbot/appointments/get-data-json?date=' + encodeURIComponent(formatDate(start)),
            dataType: "script",
            success: function( data, textStatus, jqxhr ) {          
                v_calendar.weekCalendar('refresh');
            },
            error: debugJQueryError
          });        
   }
            
   var scheduleId = jQuery('#data_source').val();
   callback(g_data[scheduleId]);
}



function onEventResize(calEvent, event) {
    alert(calEvent.id);
    jQuery.ajax({
          complete:function(request){ 
                v_calendar.weekCalendar('refresh');
          }, 
          data:'Appointments[StartTime]=' + formatDate(calEvent.start) + '&Appointments[EndTime]=' + formatDate(calEvent.end) + '&Appointments[ScheduleID]=' + calEvent.userId, 
          type:'post', 
          url: g_urlEventEdit+calEvent.id
    });
    return true;
}        
        
        

function onEventNew(calEvent, event, FreeBusyManager, calendar) {
        jQuery('#modal1').dialog('show');   
	jQuery('#create-form').prop('action', g_urlEventNew);
	jQuery('.help-block-error').empty();
        jQuery('#appointments-clientname').val('');
	jQuery('#appointments-phone').val('');
	jQuery('#appointments-mobile').val('');
	jQuery('#appointments-email').val('');
	jQuery('#appointments-starttime').val(formatDate(calEvent.start));
	jQuery('#appointments-endtime').val(formatDate(calEvent.end));
        jQuery('#appointments-scheduleid').val(calEvent.userId);
}
            
        
        
function onEdit(calEvent, event) {
    jQuery.ajax({data:'id=' + calEvent.id, success:function(request){ show_modal(request); }, type:'post', url:g_urlEventEdit+calEvent.id});        
}

v_calendar = jQuery('#calendar').weekCalendar({

      businessHours: {start: 8, end: 18, limitDisplay: true},
      readonly: false,
      showAsSeparatedUser: true,
      timeslotsPerHour: 3,
      scrollToHourMillis: 0,
      displayFreeBusys: true,
      defaultFreeBusy: {free: true},      
      height: function(v_calendar){
        return jQuery(window).height() - jQuery('h1').outerHeight(true);
      },
      eventRender : function(calEvent, event) {
        if (calEvent.end.getTime() < new Date().getTime()) {
          event.css('backgroundColor', '#aaa');
          event.find('.wc-time').css({
            backgroundColor: '#999',
            border:'1px solid #888'
          });
        }
      },
      eventNew : onEventNew,
      eventClick: onEdit,
      eventResize : onEventResize,		
      eventDrop : onEventResize,
      data: 'http://localhost:3000/erinnerungsbot/appointments/load-calendar-data',//getData,
      users: [{"id": <?= $schedule->ScheduleID ?>, "name": "<?= $schedule->Name ?>"}],
      getUserId: function(user, index, calendar) {
            return user.id;
      },
      getUserName: function(user, index, calendar) {
            return user.name;
      },		
      showAsSeparateUser: true,
      displayOddEven: true,
      daysToShow: 7,
      switchDisplay: {'1 day': 1, '3 next days': 3, 'work week': 5, 'full week': 7},
      headerSeparator: ' ',
      useShortDayNames: true,
      firstDayOfWeek: $.datepicker.regional['de'].firstDay,
      shortDays: $.datepicker.regional['de'].dayNamesShort,
      longDays: $.datepicker.regional['de'].dayNames,
      shortMonths: $.datepicker.regional['de'].monthNamesShort,
      longMonths: $.datepicker.regional['de'].monthNames,
      dateFormat: 'd F y'
    });

