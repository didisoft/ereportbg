<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LoginsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Logins');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    <?php
        echo PopoverX::widget([
            'header' => Yii::t('app', 'Help'),
            'type' => PopoverX::TYPE_INFO,
            'placement' => PopoverX::ALIGN_RIGHT,
            'content' => Yii::t('app', 'Here you can add additional users for your account.'),
            'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
        ]);	
    ?>					
</p>
<div class="box box-primary">
            <!-- form start -->

              <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'LoginID',
            //'CustomerID',
            'FirstName',            
			'LastName',
			'Email:email',			
            //'IsAdmin',
            // 
            // 'Username',
            // 'Password',
            // 'password_reset_token',
            // 'created_at',
            // 'updated_at',
            // 'auth_key',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
</div>
</div>
