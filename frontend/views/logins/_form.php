<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logins-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'FirstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true])->hint(Yii::t('app', 'Email for accessing the account')) ?>

    <?= $form->field($model, 'Password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IsAdmin')->checkbox()->hint(Yii::t('app', 'Administrative rights, to add additional users')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['logins/index']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
