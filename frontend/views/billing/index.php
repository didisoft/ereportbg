<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use frontend\widgets\HintWidget;
use kartik\popover\PopoverX;

$this->title = Yii::t('app', 'Billing');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
            <!-- form start -->

              <div class="box-body">

<div class="col-md-1">		
    <b>
		<?php echo Yii::t('app', 'Current plan') ?>
    </b>    
</div>
<div class="col-md-1">		
		<?= $plan->Name ?>
</div>
<div class="col-md-1">		
		<?php 
			echo Html::a(Yii::t('app', 'Change plan'), \Yii::$app->getUrlManager()->createUrl(['fastspring/plan']), ['class' => 'btn btn-success']);
		?>	
</div>
<div class="col-md-2">		
		<?php 
            if ($customer->PlanID != 0) {
                echo Html::a(Yii::t('app', 'Manage subscription'), $customer->FastSpringUrl,  ['target' => '_blank']);
            }
		?>	
</div>
                  
    </div><!-- settings -->
</div><!-- settings -->

