<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model app\models\Words */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
            <!-- form start -->

              <div class="box-body">


    <?php $form = ActiveForm::begin([
                   'options' => [
                    'id' => 'edit-form',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form', 
                    'enableAjaxValidation' => true,	
                    ],
                    'fieldConfig' => [
                            'options' => [
                                   'class' => 'field',
                       ]
                ]
        ]); ?>

<?php if ($model->isNewRecord) { 
	$model->StartHour = 8;
	$model->EndHour = 18;
	$model->MinLength = 30;
	$model->DefaultLength = 60;
}
?>

    <?= $form->field($model, 'Name')->textInput(['style'=>'width:300px', 'maxlength' => 255])
->hint('Name of your schedule')	?>

    <div class="field">
    <div class="two fields">
	<?= $form->field($model, 'StartHour')->dropDownList(
		[0 => '00:00',
		1 => '01:00',
		2 => '02:00',
		3 => '03:00',
		4 => '04:00',
		5 => '05:00',
		6 => '06:00',
		7 => '07:00',
		8 => '08:00',
		9 => '09:00',
		10 => '10:00',
		11 => '11:00',
		12 => '12:00',
		13 => '13:00',
		14 => '14:00',
		15 => '15:00',
		16 => '16:00',
		17 => '17:00',
		18 => '18:00',
		19 => '19:00',
		20 => '20:00',
		21 => '21:00',
		22 => '22:00',
		23 => '23:00'], 
		['style'=>'width:100px', 'class'=>'theme']);
	?>

	<?= $form->field($model, 'EndHour')->dropDownList(
		[0 => '00:00',
		1 => '01:00',
		2 => '02:00',
		3 => '03:00',
		4 => '04:00',
		5 => '05:00',
		6 => '06:00',
		7 => '07:00',
		8 => '08:00',
		9 => '09:00',
		10 => '10:00',
		11 => '11:00',
		12 => '12:00',
		13 => '13:00',
		14 => '14:00',
		15 => '15:00',
		16 => '16:00',
		17 => '17:00',
		18 => '18:00',
		19 => '19:00',
		20 => '20:00',
		21 => '21:00',
		22 => '22:00',
		23 => '23:00'], 
		['style'=>'width:100px', 'class'=>'theme']);
	?>
    </div>    
    </div>
    
	<?= $form->field($model, 'MinLength')->dropDownList(
		[5 => '5 minutes',
        10 => '10 minutes',    
		15 => '15 minutes',
        20 => '20 minutes',        
		30 => '30 minutes',
		60 => '60 minutes'], 
		['style'=>'width:150px', 'class'=>'theme']);
	?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Insert') : Yii::t('app', 'Submit'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('app', 'Cancel'), ['schedules/index']) ?>

    <?php ActiveForm::end(); ?>

                  </div>
</div>
