<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Schedules');
$this->params['breadcrumbs'][] = $this->title;
?>

	<?php 
	if ($plan->SchedulesCount > $dataProvider->count) { 
     ?>	
    <p>	
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>		
    </p>
	<?php } else { ?>
        <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                
		<?php echo Yii::t('app', 'You\'ve reached the maximum of {schedulesCount} schedules for your subscription plan',
			['schedulesCount' => $plan->SchedulesCount]); ?>
        </div>
	<?php } ?>	


<div class="box box-primary">
            <!-- form start -->

              <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Name',
            ['class' => 'yii\grid\ActionColumn', 
             'template' => '{update}&nbsp;&nbsp;{delete}',
                'buttons'=>[
                              'update' => function ($url, $model) {     
                                return Html::a(Yii::t('yii', 'Update'), $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-default fa fa-edit'

                                ]);                                
            
                              },
                                'delete' => function ($url, $model) {     
                                return Html::a(Yii::t('yii', 'Delete'), $url, [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'class' => 'btn btn-default fa fa-trash'                                    
                                  ]);                                
                                }
            ],                          
        ],
    ]
                    ]); ?>

                  </div>
</div>
