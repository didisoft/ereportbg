<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Words */

$this->title = Yii::t('app', 'New record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Schedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'New record');
?>
<div class="words-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
