<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WordsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="words-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'WordID') ?>

    <?= $form->field($model, 'CustomerID') ?>

    <?= $form->field($model, 'WordText') ?>

    <?= $form->field($model, 'WordGroupID') ?>

    <?= $form->field($model, 'EnteredBy') ?>

    <?php // echo $form->field($model, 'EnteredOn') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
