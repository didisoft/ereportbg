<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Words */

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Schedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="words-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
