<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Comment</h4>
      </div>
	  <div class="modal-body">
          
    <?php $form = 
            ActiveForm::begin([
                'options' => [
                    'id' => 'edit-schedule',
                    'autocomplete'=>'off',	
                    'class'  => 'ui form ui-front', 
                    ],
                ]);    
	?>

            
        <?= $form->errorSummary($model); ?>        

    <?= $form->field($model, 'Name')->textInput(['style'=>'width:300px', 'maxlength' => 255])
->hint('Name of your schedule')	?>

    <div class="field">
    <div class="two fields">
	<?= $form->field($model, 'StartHour')->dropDownList(
		[0 => '00:00',
		1 => '01:00',
		2 => '02:00',
		3 => '03:00',
		4 => '04:00',
		5 => '05:00',
		6 => '06:00',
		7 => '07:00',
		8 => '08:00',
		9 => '09:00',
		10 => '10:00',
		11 => '11:00',
		12 => '12:00',
		13 => '13:00',
		14 => '14:00',
		15 => '15:00',
		16 => '16:00',
		17 => '17:00',
		18 => '18:00',
		19 => '19:00',
		20 => '20:00',
		21 => '21:00',
		22 => '22:00',
		23 => '23:00'], 
		['style'=>'width:100px', 'class'=>'theme']);
	?>

	<?= $form->field($model, 'EndHour')->dropDownList(
		[0 => '00:00',
		1 => '01:00',
		2 => '02:00',
		3 => '03:00',
		4 => '04:00',
		5 => '05:00',
		6 => '06:00',
		7 => '07:00',
		8 => '08:00',
		9 => '09:00',
		10 => '10:00',
		11 => '11:00',
		12 => '12:00',
		13 => '13:00',
		14 => '14:00',
		15 => '15:00',
		16 => '16:00',
		17 => '17:00',
		18 => '18:00',
		19 => '19:00',
		20 => '20:00',
		21 => '21:00',
		22 => '22:00',
		23 => '23:00'], 
		['style'=>'width:100px', 'class'=>'theme']);
	?>
    </div>    
    </div>
    
	<?= $form->field($model, 'MinLength')->dropDownList(
		[5 => '5 minutes',
        10 => '10 minutes',    
		15 => '15 minutes',
        20 => '20 minutes',        
		30 => '30 minutes',
		60 => '60 minutes'], 
		['style'=>'width:150px', 'class'=>'theme']);
	?>

        
	  </div>
      <div class="modal-footer">
            <?= Html::button(Yii::t('app', 'Submit'), ['onclick' => "submitEditScheduleForm(); return true;", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>        

	  <?php 
        ActiveForm::end(); 
      ?>
 		</div>
	</div>    
	
<script>
//<![CDATA[    
submitEditScheduleForm = function() {
	var _form = $('#edit-schedule');
	var _data = _form.serialize();
	 $.ajax({
		  url: _form.attr('action'),
		  type: 'post',
		  data: _data,
		  success: function (response) {
				$('#modalEditSchedule').modal('hide');
                onEditScheduleEnd();
		  },
	 });
}
$('#modalEdit').on('submit', 'form', function(event) {
		event.preventDefault();
		 
		 submitEditScheduleForm();
		 
		return false;
});
//]]>
</script>

