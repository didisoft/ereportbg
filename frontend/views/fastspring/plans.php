<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Subscription plan');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->homeUrl ?>plans-style.css" />
<div class="settings">

    <?php $form = ActiveForm::begin(); ?>
	
  <div class="plans">
	<?php if ($plan4 != null) { ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan4->Name ?></h3>
      <p class="plan-price"><?php echo $plan4->Price ?><span class="plan-unit"><?= Yii::t('app', 'Euro per Month')?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan4->AppointmentsCount ?> <span class="plan-feature-name"><?= Yii::t('app', 'appointments per month')?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan4;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 			
		}
	  ?>
    </div>
	<?php } ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan3->Name ?></h3>
      <p class="plan-price"><?php echo $plan3->Price ?><span class="plan-unit"><?= Yii::t('app', 'Euro per Month')?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan3->AppointmentsCount ?> <span class="plan-feature-name"><?= Yii::t('app', 'appointments per month')?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan3;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 			
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan2->Name ?></h3>
      <p class="plan-price"><?php echo $plan2->Price ?><span class="plan-unit"><?= Yii::t('app', 'Euro per Month')?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan2->AppointmentsCount ?> <span class="plan-feature-name"><?= Yii::t('app', 'appointments per month')?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan2;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 			
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan1->Name ?></h3>
      <p class="plan-price"><?php echo $plan1->Price ?><span class="plan-unit"><?= Yii::t('app', 'Euro per Month')?></span></p>
      <ul class="plan-features">
          <li class="plan-feature"><?php echo $plan1->AppointmentsCount ?> <br/><div class="plan-feature-name"><?= Yii::t('app', 'appointments per month')?></div></li>
      </ul>
      <?php 
		$tmpPlan = $plan1;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 			
		}
		?>
    </div>
  </div>
	

    <?php ActiveForm::end(); ?>

</div><!-- settings -->
