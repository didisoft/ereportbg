<?php
namespace frontend\components;

//use \Google\Client;
//use \Google\Service\Customsearch;
//Yii::import('application.components.Google.Service.*');

use common\models\Options;

use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

class Twillio {

        // Twilio REST API version
        public $version = "2010-04-01";

        // Set our Account SID and AuthToken
        public $sid;
        public $token;
	
        // A phone number you have previously validated with Twilio
        public $system_phonenumber = '+35992919029';
        
        // Instantiate a new Twilio Rest Client
        public $client = null;
        
        
	function getClient() {
		return new \Services_Twilio($this->sid, $this->token, $this->version);
	}

    /**
     * 
     * @param type $number Must be E164 formatted
     * @param type $message_url
     * @param type $status_url
     * @param type $countryCode
     * @return boolean
     */
	function call($number, $message_url, $status_url='', $countryCode='BG') {            
            // Initiate a new outbound call
            $client = $this->getClient();
            
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            try {
                $num = $phoneNumberUtil->parse($number, $countryCode);
                $number = $phoneNumberUtil->format($num, PhoneNumberFormat::E164);
            } catch (\libphonenumber\NumberParseException $e) {
                $number = $phoneNumberUtil->format($number, PhoneNumberFormat::E164);
            }
            
            $system_phone_number = Options::getOption(Option_SystemPhoneNumber);
            
            $call = $client->account->calls->create(
                $system_phone_number, // The number of the phone initiating the call
                $number, //, // The number of the phone receiving call 
                $message_url, // The URL Twilio will request when the call is answered
                array('IfMachine'=>'Continue',
                        "StatusCallback" => $status_url,
                        "StatusCallbackMethod" => "POST",
                        "StatusCallbackEvent" => "busy",                    
                        "StatusCallbackEvent" => "no-answer",
                        "StatusCallbackEvent" => "canceled",
                        "StatusCallbackEvent" => "failed",
                        "StatusCallbackEvent" => "completed",
                    )    
            );
            return true;
	}	
	
    function sms($number, $message, $countryCode='BG') {
            $client = $this->getClient();
            
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            try {
                $num = $phoneNumberUtil->parse($number, $countryCode);
                $number = $phoneNumberUtil->format($num, PhoneNumberFormat::E164);
            } catch (\libphonenumber\NumberParseException $e) {
                $number = $phoneNumberUtil->format($number, PhoneNumberFormat::E164);
            }

            $system_mobile_number = Options::getOption(Option_SystemMobileNumber);
            
            $client->account->messages->sendMessage(
               $system_mobile_number, // From a Twilio number in your account
               $number, // Text any number $number
               $message   // Message body (if any)
           );            
           return true;
	}		
    
    function isMobileNumber($number, $countryCode) {
        $client = new \Lookups_Services_Twilio($this->sid, $this->token);
        $number = $client->phone_numbers->get($number, array("CountryCode" => $countryCode, "Type" => "carrier"));

        return $number->carrier->type == 'mobile';    
    }
}
?>