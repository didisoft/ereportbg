<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class HintWidgetAsset extends \yii\web\AssetBundle {
    public $depends = [
        'yii\web\JqueryAsset',
    ];
	
    public function init()
    {
        $this->sourcePath = __DIR__.'/assets';
        $this->css = ['hint.css'];
        $this->js = ['hint.js'];
        parent::init();
	}
}
?>