<?php
namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class HintWidget extends Widget{
	public $message;
	
	public function init(){
		parent::init();
	}
	
	public function run(){
	?> 	
	<span class="hint">
		<img class="trigger" src="<?php echo Yii::$app->request->baseUrl; ?>/images/help.gif" />
		<div class="popup">
			<?php echo Html::encode($this->message); ?>
		</div>	
	</span>	
	<?php	
		$view = $this->getView();
        HintWidgetAsset::register($view);
	}
}
?>