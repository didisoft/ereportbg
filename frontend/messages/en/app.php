<?php
return array (	
	'Add' =>'',
	'Address' =>'',
	'Amount' =>'',
	'Advanced Options' =>'',
	'Cancel' =>'',
	'City' =>'',
	'Change plan' =>'',	
	'Check your email for further instructions.' =>'',
	'Company Name' =>'',
	'Continue' =>'',
	'Custom' =>'',
	'Data saved' =>'',
	'Details' =>'',
	'EIK' =>'',
	'Edit' =>'',
	'Entered By' =>'',
	'Entered On' =>'',
	'Email' =>'',
	'Error saving data' =>'',
	'ExactMatch' =>'Exact Match',
	'ExcludeTerms' =>'',
	'Extend subscription' =>'',
	'Expired' =>'',
	'Favorites' =>'',
	'From' =>'',	
	'First Name' =>'',
	'Home' =>'',	
	'How much of the available words/phrases for tracked you are using' =>'',
	'IN_DDS' =>'',
	'Information' =>'',	
	'Insert' =>'',	
	'Invoice' =>'',	
	'Invoices' =>'',	
	'Company details' =>'',
	'Is Admin' =>'',
	'Issued Invoices' =>'',
	'Issued Invoices Archive' =>'',
	'Invoice Date' =>'',
	'Incorrect username or password.' =>'',
	'Last 7 Days' =>'',
	'Last 30 days' =>'',
	'Last Name' =>'',
	'Link' =>'',
	'Login' =>'',
	'Logins' =>'',
	'Logout' =>'',
	'Lost password' =>'',
	'Mentions' =>'',
	'Mention Date' =>'',
	'MentionsSchedule' =>'Tracking Schedule',
	'MentionsNoEmail' =>'',
	'Mentions by Email' =>'',
	'MOL' =>'',
	'New record' =>'',
	'New password' =>'',
	'New password was saved.' =>'',
	'Optional' =>'',
	'Order Text' =>'',
	'OrTerms' =>'',
	'Next Billing date' =>'',
	'Password' =>'',
	'Period' =>'',
	'Please activate your account.' =>'',
	'Please fill out your email. A link to reset password will be sent there.' =>'',
	'Please choose your new password:' =>'',
	'Print' =>'',
	'Registration' =>'',
	'Remember me' =>'',	
	'Recover lost password' =>'',
	'Settings' =>'',
	'Select' =>'',
	'Select only if you want the monitored term to be exactly matched'=>'',
	'Specify here extra terms that you want to be found in the results' => '',
	'Specify here words or phrases that shouldn\'t be found in the results' => '',	
	'Send' =>'',
	'Signup' =>'',	
	'Show' =>'',
	'Snippet' =>'',	
	'Source' => '',
	'Sorry, we are unable to reset password for email provided.' =>'',
	'Subscription' =>'',		
	'Subscription plan' =>'',
	'Submit' =>'',
	'The text of already entered words cannot be edited. If no longeed need a word/phrase, just delete it and then add a new one'=>'',
	'This month' =>'',	
	'Title' =>'',	
	'Terms' =>'',		
	'Terms apply' =>'',
	'To' =>'',		
	'Today' =>'',
	'The new plan becomes active after successful order' =>'',
	'Update' =>'',
	'Username' =>'',
	'Word' =>'',
	'Schedules' =>'',
        'Welcome to' => '',
	'Words tracked' =>'',
	'Word Text' =>'',	
	'Yesterday' =>'',
	'Your current subscription plan' =>'',
	'You\'ve reached the maximum of {wordsCount} words for your subscription plan'=>'',
	
'Everyday' =>'', 
'Monday' =>'', 
'Tuesday' =>'',
'Wednesday' =>'',
'Thursday' =>'',
'Friday' =>'',
'Saturday' =>'',
'Sundey' =>'',
	
);