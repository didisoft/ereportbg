<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class CalendarAsset extends AssetBundle
{
public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $js = [
		'fullcalendar/moment.js',
        'fullcalendar/moment-with-locales.js',
        'fullcalendar/fullcalendar.min.js',
        'fullcalendar/de.js',
        // more plugin Js here
    ];
    public $css = [
        'fullcalendar/fullcalendar.css',
        // more plugin CSS here
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];}
