<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = 'Получено плащане';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['billing/invoice-external', 'id' => $id, 'key' => $key]);
?>
<div class="password-reset">
    <p>Здравейте,</p>

    <p>Благодарим Ви за заплатения абонамент за услугите на <?= \Yii::$app->params['siteName'] ?>
	<br/>
	Новият Ви абонамент за план "<?= $plan ?>" ще е валиден до <?= $expiration ?>.
	</p>

	<p>Фактура за направеното плащане можете да изтеглите от тук:</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>