<?php
namespace common\helpers;

use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationHelper
 *
 * @author Nasko
 */
class NotificationHelper {
    public static function selfNotificationValues($verifiedPhone=false) {
        if ($verifiedPhone) {
        return [
            Notification_Type_Email => \Yii::t('app', 'Email'),    
            Notification_Type_SMS => \Yii::t('app', 'SMS'),    
            Notification_Type_Email_And_SMS => \Yii::t('app', 'Email and SMS')];
        } else {
        return [
            Notification_Type_Email => \Yii::t('app', 'Email')
         ];            
        }
    }
    
    public static function notificationValues() {
        return 		[
        0 => \Yii::t('app', 'Don\'t send'),    
        -8 => Yii::t('app', 'At 8:00 on the morning of their appointment'),    
        30 => Yii::t('app', '30 minutes before appointment'),
		60 => Yii::t('app', '1 hour before appointment'),
		2*60 => Yii::t('app', '2 hours before appointment'),
		3*60 => Yii::t('app', '3 hours before appointment'),
		4*60 => Yii::t('app', '4 hours before appointment'),
		5*60 => Yii::t('app', '5 hours before appointment'),
		24*60 => Yii::t('app', '1 day before appointment'),
		2*24*60 => Yii::t('app', '2 days before appointment'),
		3*24*60 => Yii::t('app', '3 days before appointment'),
		4*24*60 => Yii::t('app', '4 days before appointment'),
		5*24*60 => Yii::t('app', '5 days before appointment'),
        6*24*60 => Yii::t('app', '6 days before appointment'),    
        7*24*60 => Yii::t('app', '7 days before appointment'),
        14*24*60 => Yii::t('app', '14 days before appointment')];
    }
}
