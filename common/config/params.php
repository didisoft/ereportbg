<?php
return [
    'adminEmail' => 'kontakt@erinnerungsbot.de',
	'siteName' => 'ErinnerungsBot.de',
    'supportEmail' => 'kontakt@erinnerungsbot.de',
    'user.passwordResetTokenExpire' => 3600,
];
