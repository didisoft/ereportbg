<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $OrderID
 * @property string $OrderDate
 * @property integer $CustomerID
 * @property integer $PlanID
 * @property integer $Period
 * @property string $Amount
 * @property string $MOL
 * @property string $EIK
 * @property string $Address
 * @property string $CompanyName
 * @property integer $PaymentMethodID
 * @property integer $OrderStatusID
 * @property string $IN_DDS
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderDate', 'CustomerID', 'PlanID', 'Period', 'Amount', 'PaymentMethodID', 'OrderStatusID'], 'required'],
            [['OrderDate'], 'safe'],
            [['CustomerID', 'PlanID', 'Period', 'PaymentMethodID', 'OrderStatusID'], 'integer'],
            [['Amount'], 'number'],
            [['MOL', 'Address', 'CompanyName'], 'string', 'max' => 255],
            [['EIK', 'IN_DDS'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OrderID' => Yii::t('app', 'Order ID'),
            'OrderDate' => Yii::t('app', 'Order Date'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'PlanID' => Yii::t('app', 'Plan ID'),
            'Period' => Yii::t('app', 'Period'),
            'Amount' => Yii::t('app', 'Amount'),
            'MOL' => Yii::t('app', 'Mol'),
            'EIK' => Yii::t('app', 'Eik'),
            'Address' => Yii::t('app', 'Address'),
            'CompanyName' => Yii::t('app', 'Company Name'),
            'PaymentMethodID' => Yii::t('app', 'Payment Method ID'),
            'OrderStatusID' => Yii::t('app', 'Order Status ID'),
            'IN_DDS' => Yii::t('app', 'In  Dds'),
        ];
    }
}
