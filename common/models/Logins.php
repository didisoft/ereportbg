<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logins".
 *
 * @property integer $LoginID
 * @property integer $CustomerID
 * @property string $FirstName
 * @property string $Email
 * @property integer $IsAdmin
 * @property string $LastName
 * @property string $Username
 * @property string $Password
 */
class Logins extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'FirstName', 'Email', 'LastName', 'Password'], 'required'],
            [['CustomerID', 'IsAdmin', 'MentionsSchedule', 'MentionsNoEmail'], 'integer'],
            [['FirstName', 'Email', 'LastName', 'Password'], 'string', 'max' => 255],
			[['LastMentionsSchedule'], 'safe'],
			[['Email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LoginID' => 'Login ID',
            'CustomerID' => 'Customer ID',
            'FirstName' => Yii::t('app', 'First Name'),
            'Email' => Yii::t('app', 'Email'),
            'IsAdmin' => Yii::t('app', 'Admin rights'),
            'LastName' => Yii::t('app', 'Last Name'),
            'Password' => Yii::t('app', 'Password'),
			'MentionsSchedule' => Yii::t('app', 'MentionsSchedule'),
			'MentionsNoEmail' => Yii::t('app', 'MentionsNoEmail'),
        ];
    }
	
}
