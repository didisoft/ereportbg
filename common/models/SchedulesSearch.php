<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Schedules;

/**
 * SchedulesSearch represents the model behind the search form about `app\models\Schedules`.
 */
class SchedulesSearch extends Schedules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ScheduleID', 'CustomerID', 'DefaultLength'], 'integer'],
            [['Name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $customerId)
    {
        $query = Schedules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
			$this->CustomerID = $customerId;
            return $dataProvider;
        }

        $query->andFilterWhere([
            'WordID' => $this->WordID,
            'CustomerID' => $this->CustomerID,
            'DefaultLength' => $this->DefaultLength,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name]);

        return $dataProvider;
    }
}
