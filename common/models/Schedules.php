<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "schedules".
 *
 * @property integer $ScheduleID
 * @property integer $CustomerID
 * @property string $Name
 * @property integer $StartHour
 * @property integer $EndHour
 * @property integer $MinLength
 */
class Schedules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'Name', 'StartHour', 'EndHour', 'MinLength'], 'required'],
            [['CustomerID', 'StartHour', 'EndHour', 'MinLength', 'Active'], 'integer'],
			[['EmailBody', 'PhoneText'], 'string'],
            [['Name', 'SMSText'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ScheduleID' => Yii::t('app', 'Schedule ID'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'Name' => Yii::t('app', 'Name'),
            'StartHour' => Yii::t('app', 'Start Hour'),
            'EndHour' => Yii::t('app', 'End Hour'),
            'MinLength' => Yii::t('app', 'Minimal Appointment Length'),
        ];
    }
}
