<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property integer $OptionID
 * @property string $Name
 * @property string $Value
 * @property string $Description
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Value'], 'required'],
            [['Value'], 'string'],
            [['Name', 'Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OptionID' => Yii::t('app', 'Option ID'),
            'Name' => Yii::t('app', 'Name'),
            'Value' => Yii::t('app', 'Value'),
            'Description' => Yii::t('app', 'Description'),
        ];
    }
    
    public static function getOption($name) {
        return Options::findOne(['Name' => $name])->Value;
    }
}
