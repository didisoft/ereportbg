<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property integer $PlanID
 * @property string $Name
 * @property integer $WordsCount
 * @property integer $LoginsCount
 * @property string $slug
 * @property string $Price
 * @property string $Price3m
 * @property string $Price6m
 * @property string $Price12m
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PlanID', 'Name', 'AppointmentsCount', 'SchedulesCount', 'slug', 'Price', 'Price3m', 'Price6m', 'Price12m'], 'required'],
            [['PlanID', 'AppointmentsCount', 'SchedulesCount'], 'integer'],
            [['Price', 'Price3m', 'Price6m', 'Price12m'], 'number'],
            [['Name', 'FastSpringProduct'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PlanID' => Yii::t('app', 'Plan ID'),
            'Name' => Yii::t('app', 'Name'),
            'WordsCount' => Yii::t('app', 'Words Count'),
            'LoginsCount' => Yii::t('app', 'Logins Count'),
            'slug' => Yii::t('app', 'Slug'),
            'Price' => Yii::t('app', 'Price'),
            'Price3m' => Yii::t('app', 'Price3m'),
            'Price6m' => Yii::t('app', 'Price6m'),
            'Price12m' => Yii::t('app', 'Price12m'),
        ];
    }
}
