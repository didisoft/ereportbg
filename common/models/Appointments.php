<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "appointments".
 *
 * @property integer $AppointmentID
 * @property integer $ScheduleID
 * @property string $AppointmentTime
 * @property integer $ClientID
 * @property string $Email
 * @property string $Phone
 * @property integer $StatusID
 */
class Appointments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ScheduleID', 'StartTime', 'EndTime', 'ClientID', 'Email', 'Phone', 'StatusID'], 'required'],
            [['ScheduleID', 'ClientID', 'CustomerID', 'StatusID', 'IsScheduled'], 'integer'],
            [['StartTime', 'EndTime', 'ClientName'], 'safe'],
            [['Email', 'Phone', 'Mobile', 'ClientName'], 'string', 'max' => 255],
            ['Phone', 'validatePhone']
        ];
    }

	public function getClient()
    {
      return $this->hasOne(\common\models\Clients::className(), array('ClientID' => 'ClientID'));
    }
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AppointmentID' => Yii::t('app', 'Appointment ID'),
            'ScheduleID' => Yii::t('app', 'Schedule ID'),
            'AppointmentTime' => Yii::t('app', 'Appointment Time'),
            'ClientID' => Yii::t('app', 'Client ID'),
            'Email' => Yii::t('app', 'Email'),
            'Phone' => Yii::t('app', 'Phone'),
            'StatusID' => Yii::t('app', 'Status ID'),
        ];
    }
    
    public function validatePhone()
    {
        $util = \libphonenumber\PhoneNumberUtil::getInstance();

        if (!$util->isViablePhoneNumber($this->Phone)) {
            $this->addError('Phone', Yii::t('app', 'The specified phone number does not seem to be valid'));
        }
    }        
}
