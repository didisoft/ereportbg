<?php

namespace common\models;

use Yii;
use \common\models\Plans;

/**
 * This is the model class for table "customers".
 *
 * @property integer $CustomerID
 * @property string $CompanyName
 * @property integer $PlanID
 * @property string $RegistrationTime
 * @property string $PlanExpirationTime
 * @property integer $Active
 * @property string $Discount
 * @property string $activkey
 * @property string $Address
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PlanID', 'Active', 'Test', 'PlanActive', 'IntervalMinutesEmail', 'IntervalMinutesSMS', 'IntervalMinutesPhone', 'StartHour', 'EndHour', 'MinLength', 'NotificationPrefs'], 'integer'],
            [['RegistrationTime', 'PlanExpirationTime'], 'safe'],
            [['Discount', 'Notified'], 'number'],
            [['CompanyName', 'City', 'activkey', 'FastSpringUrl', 'FastSpringRef', 'TimeZone'], 'string', 'max' => 255],
            [['EmailSubject', 'SMSText', 'SmsConfirmText', 'SmsCancelText', 'SmsCallbackText', 'EmailFrom', 'PhoneFrom'], 'string', 'max' => 255],
            [['EmailBody', 'PhoneText', 'PhoneConfirmText', 'PhoneCancelText', 'PhoneCallbackText', 'EmailConfirmText', 'EmailCancelText'], 'string'],
            [['Address'], 'string', 'max' => 1000],
            [['CountryCode'], 'string', 'max' => 2],
        ];
    }

    public function getPlan()
    {
      return $this->hasOne(\common\models\Plans::className(), array('PlanID' => 'PlanID'));
    }
   
    
	public function getLogins()
    {
        return Logins::findAll(['CustomerID' => $this->CustomerID]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CustomerID' => 'Customer ID',
            'CompanyName' => Yii::t('app', 'Company Name'),
            'PlanID' => 'Plan ID',
            'RegistrationTime' => 'Registration Time',
            'PlanExpirationTime' => 'Plan Expiration Time',
            'Active' => 'Active',
            'Discount' => 'Discount',
            'activkey' => 'Activkey',
			'City' => Yii::t('app', 'City'),
            'Address' => Yii::t('app', 'Address'),
            'MOL' => Yii::t('app', 'MOL'),
            'EIK' => Yii::t('app', 'EIK'),
            'IN_DDS' => Yii::t('app', 'IN_DDS'),
            'SMSText' => Yii::t('app', 'SMS Text'),
            'MinLength' => Yii::t('app', 'Minimal Appointment Length'),
            'EmailSubject' => Yii::t('app', 'Email subject for an appointment notification'),
            'EmailBody' => Yii::t('app', 'Email text for an appointment notification'),
            'EmailConfirmText' => Yii::t('app', 'Email Response when customer confirms an appointment'),
            'EmailCancelText' => Yii::t('app', 'Email Response when customer cancels an appointment'),
            'PhoneText' => Yii::t('app', 'Phone call message for an appointment notification'),
            'PhoneCallbackText' => Yii::t('app', 'Phone call response when customer requests callback'),
            'PhoneConfirmText' => Yii::t('app', 'Phone call response when customer confirms appointment'),
            'PhoneCancelText' => Yii::t('app', 'Phone call response when customer cancels appointment'),
            'IntervalMinutesEmail' => Yii::t('app', 'Email notification timing'),
            'IntervalMinutesSMS' => Yii::t('app', 'SMS notification timing'),
            'IntervalMinutesPhone' => Yii::t('app', 'Phone call notification timing'),
            'NotificationPrefs' => Yii::t('app', 'Contact request notification'),
        ];
    }
}
