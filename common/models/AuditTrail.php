<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "audit_trail".
 *
 * @property integer $audit_trail_id
 * @property string $app_name
 * @property string $method_name
 * @property integer $tenant_id
 * @property string $user_name
 * @property string $event_date
 * @property string $params
 * @property integer $result
 * @property string $client_ip
 * @property string $exception_info
 * @property string $exception_detail
 */
class AuditTrail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_trail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_name', 'user_name'], 'required'],
            [['tenant_id', 'result'], 'integer'],
            [['event_date'], 'safe'],
            [['params', 'exception_detail'], 'string'],
            [['app_name', 'method_name'], 'string', 'max' => 256],
            [['user_name', 'client_ip'], 'string', 'max' => 64],
            [['exception_info'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'audit_trail_id' => 'Audit Trail ID',
            'app_name' => 'App Name',
            'method_name' => 'Method Name',
            'tenant_id' => 'Tenant ID',
            'user_name' => 'User Name',
            'event_date' => 'Event Date',
            'params' => 'Params',
            'result' => 'Result',
            'client_ip' => 'Client Ip',
            'exception_info' => 'Exception Info',
            'exception_detail' => 'Exception Detail',
        ];
    }
}
