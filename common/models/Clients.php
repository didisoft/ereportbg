<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $ClientID
 * @property string $Name
 * @property string $Email
 * @property string $Phone
 * @property string $Mobile
 * @property integer $SendEmail
 * @property integer $SendPhone
 * @property integer $SendMobile
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Email', 'Phone', 'SendEmail', 'SendPhone', 'SendMobile'], 'required'],
            [['SendEmail', 'SendPhone', 'SendMobile', 'CustomerID'], 'integer'],
            [['Name', 'Email', 'Phone', 'Mobile'], 'string', 'max' => 255],
            ['Email', 'email'],
            [['Phone', 'Mobile'], 'validatePhone']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ClientID' => Yii::t('app', 'Client ID'),
            'Name' => Yii::t('app', 'Name'),
            'Email' => Yii::t('app', 'Email'),
            'Phone' => Yii::t('app', 'Phone'),
            'Mobile' => Yii::t('app', 'Mobile'),
            'SendEmail' => Yii::t('app', 'Send Email'),
            'SendPhone' => Yii::t('app', 'Send Phone'),
            'SendMobile' => Yii::t('app', 'Send Mobile'),
        ];
    }    
    
    
    public function validatePhone($attribute, $params)
    {
        $util = \libphonenumber\PhoneNumberUtil::getInstance();

        if (!$util->isViablePhoneNumber($this->$attribute)) {
            $this->addError($attribute, Yii::t('app', 'The specified phone number does not seem to be valid'));
        }
    }    
}
