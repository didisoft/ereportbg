<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Appointments;

/**
 * AppointmentsSearch represents the model behind the search form about `common\models\Appointments`.
 */
class AppointmentsSearch extends Appointments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AppointmentID', 'ScheduleID', 'ClientID', 'StatusID', 'CustomerID', 'IsScheduled'], 'integer'],
            [['StartTime', 'EndTime', 'Email', 'Phone', 'Mobile', 'ClientName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appointments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AppointmentID' => $this->AppointmentID,
            'ScheduleID' => $this->ScheduleID,
            'StartTime' => $this->StartTime,
            'EndTime' => $this->EndTime,
            'ClientID' => $this->ClientID,
            'StatusID' => $this->StatusID,
            'CustomerID' => $this->CustomerID,
            'IsScheduled' => $this->IsScheduled,
        ]);

        $query->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Phone', $this->Phone])
            ->andFilterWhere(['like', 'Mobile', $this->Mobile])
            ->andFilterWhere(['like', 'ClientName', $this->ClientName]);

        return $dataProvider;
    }
}
