<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notificationspipe".
 *
 * @property integer $ID
 * @property integer $ClientID
 * @property string $FireTime
 * @property integer $Type
 * @property string $Subject
 * @property string $Body
 * @property integer $StatusID
 */
class NotificationsPipe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificationspipe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ClientID', 'EmailSubject', 'EmailBody', 'StatusID'], 'required'],
            [['ClientID', 'SendEmail', 'SendSMS', 'SendPhone', 'Attempts', 'StatusID'], 'integer'],
            [['FireDate', 'FireTime'], 'safe'],
            [['EmailBody'], 'string'],
            [['EmailSubject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ClientID' => 'Client ID',
            'FireTime' => 'Fire Time',
            'Type' => 'Type',
            'EmailSubject' => 'Subject',
            'EmailBody' => 'Body',
            'StatusID' => 'Status ID',
        ];
    }
    
    public function getClient()
    {
      return $this->hasOne(\common\models\Clients::className(), array('ClientID' => 'ClientID'));
    }    
    
    public function getCustomer()
    {
      return $this->hasOne(\common\models\Customers::className(), array('CustomerID' => 'CustomerID'));
    }    
    
}
