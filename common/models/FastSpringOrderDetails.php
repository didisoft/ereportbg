<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fastspringorderdetails".
 *
 * @property integer $ID
 * @property integer $OrderID
 * @property string $FastSpringRef
 * @property string $FastSpringUrl
 */
class FastSpringOrderDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fastspringorderdetails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderID', 'FastSpringRef', 'FastSpringUrl'], 'required'],
            [['OrderID'], 'integer'],
            [['FastSpringRef', 'FastSpringUrl'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrderID' => 'Order ID',
            'FastSpringRef' => 'Fast Spring Ref',
            'FastSpringUrl' => 'Fast Spring Url',
        ];
    }
}
