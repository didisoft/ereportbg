<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Customers;

/**
 * CustomersSearch represents the model behind the search form about `app\models\Customers`.
 */
class CustomersSearch extends Customers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'PlanID', 'Active'], 'integer'],
            [['CompanyName', 'RegistrationTime', 'PlanExpirationTime', 'activkey', 'Address', 'MOL', 'EIK', 'IN_DDS'], 'safe'],
            [['Discount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'CustomerID' => $this->CustomerID,
            'PlanID' => $this->PlanID,
            'RegistrationTime' => $this->RegistrationTime,
            'PlanExpirationTime' => $this->PlanExpirationTime,
            'Active' => $this->Active,
            'Discount' => $this->Discount,
        ]);

        $query->andFilterWhere(['like', 'CompanyName', $this->CompanyName])
            ->andFilterWhere(['like', 'activkey', $this->activkey])
            ->andFilterWhere(['like', 'Address', $this->Address])
            ->andFilterWhere(['like', 'MOL', $this->MOL])
            ->andFilterWhere(['like', 'EIK', $this->EIK])
            ->andFilterWhere(['like', 'IN_DDS', $this->IN_DDS]);

        return $dataProvider;
    }
}
