<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class ARTenant extends \yii\db\ActiveRecord
{
    public function beforeSave($insert)
    {
        $tenant = $this->getTenant();
        $this->CustomerID = $tenant;
        return parent::beforeSave($insert);
    }

	
	
    //before deletion check for the ownership ::Rajith::
    //not working for deleteAllByAttributes
    public function beforeDelete()
    { 
		$tenant = $this->getTenant();
		if ($this->CustomerID == $tenant)
		{
			return true;
		}
		else
		{
			return false; // prevent actual DELETE query from being run
		}
    }
 
 
	var $_id;
    //to get the unique UNIQUE identifier
    public function getTenant()
    {
		//this is the unique identifier . Use your own ideas to get a unique identifier(tenent)
		return $this->_id;
    }
	
    public function setTenant($v)
    {
		return $this->_id = $v;
    }
}
