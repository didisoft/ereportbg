<?php
namespace common\controllers;

use Yii;
use yii\web\Controller;
use yii\log\Logger;
use yii\helpers\Html;

use common\models\Orders;
use common\models\Customers;
use common\models\Plans;
use common\models\Logins;
use common\models\Options;

define('Max_Notification_Attempts', '1');

define('Notification_Type_Email', '1');
define('Notification_Type_SMS', '2');
define('Notification_Type_Email_And_SMS', '3');
define('Notification_Type_Phone', '4');

define('Contact_Status_Pending', '0');
define('Contact_Status_Done', '1');

define('Pipe_Status_Pending', '0');
define('Pipe_Status_Fired', '1');
define('Pipe_Status_Error', '-1');

define('Appointment_Status_Scheduled', '0');
define('Appointment_Status_Not_Notified', '2');
define('Appointment_Status_Notified', '4');
define('Appointment_Status_Confirmed', '5');
define('Appointment_Status_Canceled', '6');
define('Appointment_Status_Contact', '7');
define('Appointment_Status_Showed', '8');
define('Appointment_Status_Not_Showed', '9');


// PaymentMethodID
define ('PAYMENT_METHOD_BANK', '1');
define ('PAYMENT_METHOD_EPAY', '2');
define ('PAYMENT_METHOD_FASTSPRING', '3');
define ('PAYMENT_METHOD_MANUAL', '9');

// Events.EventType
define ('EVENT_LOGIN', '1');
define ('EVENT_REGISTER', '2');
define ('EVENT_ADD_WORD', '3');
define ('EVENT_CHANGE_WORD', '4');
define ('EVENT_DELETE_WORD', '5');
define ('EVENT_ORDER_START', '6');
define ('EVENT_ORDER_END', '7');
define ('EVENT_SEARCH', '9');

// OrderStatusID
define ('ORDER_PENDING', '0');
define ('ORDER_PAID', '1');
define ('ORDER_CANCELED', '-1');

define ('MAX_PLAN_ID', '4');

// Options
define ('Option_EmailSubject', 'EmailSubject');
define ('Option_EmailBody', 'EmailBody');
define ('Option_EmailAntispamText', 'EmailAntispamText');
define ('Option_EmailCancelText', 'EmailCancelText');
define ('Option_EmailConfirmText', 'EmailConfirmText');
define ('Option_PhoneOptionsText', 'PhoneOptionsText');
define ('Option_PhoneOptionsNoChoice', 'PhoneOptionsNoChoice');
define ('Option_SmsText', 'SmsText');
define ('Option_SmsConfirmText', 'SmsConfirmText');
define ('Option_SmsCancelText', 'SmsCancelText');
define ('Option_SmsCallbackText', 'SmsCallbackText');
define ('Option_PhoneConfirmText', 'PhoneConfirmText');
define ('Option_PhoneText', 'PhoneText');
define ('Option_PhoneCancelText', 'PhoneCancelText');
define ('Option_PhoneCallbackText', 'PhoneCallbackText');
define ('Option_DefaultTimeZone', 'DefaultTimeZone');
define ('Option_TermsUrl', 'termsUrl');
define ('Option_DefaultScheduleName', 'DefaultScheduleName');
define ('Option_SystemMobileNumber', 'SystemMobileNumber');
define ('Option_SystemPhoneNumber', 'SystemPhoneNumber');

define('Placeholder_Company', '{Unternehmen}');
define('Placeholder_Client', '{Kundenname}');
define('Placeholder_AppointmentTime', '{Termin}');
define('Placeholder_Schedule', '{Terminplan}');

/**
 * Base controller
 */
class MyController extends Controller
{
	private function eventTypeText($eventType) {
		if (EVENT_LOGIN == $eventType) return 'LOGIN';
		if (EVENT_REGISTER == $eventType) return 'REGISTER';
		if (EVENT_ADD_WORD == $eventType) return 'ADD_WORD';
		if (EVENT_CHANGE_WORD == $eventType) return 'CHANGE_WORD';
		if (EVENT_DELETE_WORD == $eventType) return 'DELETE_WORD';
		if (EVENT_ORDER_START == $eventType) return 'ORDER_START';
		if (EVENT_ORDER_END == $eventType) return 'ORDER_END';
		if (EVENT_SEARCH == $eventType) return 'SEARCH';
		
		return 'UNKNOWN '.$eventType;
	}
	
	protected function event($customerId, $eventType, $note='')
	{
        $audit = new \common\models\AuditTrail();
        $audit->app_name = Yii::$app->name;
        $audit->method_name = Yii::$app->request->url;
        $audit->tenant_id = Yii::$app->user->identity->CustomerID;
        $audit->user_name = Yii::$app->user->identity->Email;
        $audit->event_date = new \yii\db\Expression('NOW()');
        $audit->params = '';
        $audit->result = '1';
        $audit->client_ip = Yii::$app->request->userIP;
        $audit->exception_info = '';
        $audit->exception_detail = '';
        $audit->save(false);
	}

	protected function trace($var) {
		return $this->debug($var, Logger::LEVEL_TRACE);		
	}
	
	protected function error($message) {
		$this->debug($message, Logger::LEVEL_ERROR);		
	}			

	protected function debug($var, $level=Logger::LEVEL_TRACE) {
		Yii::getLogger()->log(var_export($var, true),
				$level,
				'app');	
	}		
	
    protected function getStatusCssClass($statusId) {    
        switch ($statusId) {
            case Appointment_Status_Scheduled : return 'scheduled';
            case Appointment_Status_Not_Notified : return 'not_notified';
            case Appointment_Status_Notified : return 'notified';
            case Appointment_Status_Confirmed : return 'confirmed';
            case Appointment_Status_Canceled : return 'canceled';
            case Appointment_Status_Contact : return 'requested_contact';
            case Appointment_Status_Showed : return 'showed';
            case Appointment_Status_Not_Showed : return 'no_showed';
            default : return 'scheduled';
        }
    }
    
    
    
    public function formatSms($notification) {
        $sms_message = $notification['SMSText'];
        return $this->formatNotification($notification, $sms_message);
    }

    
    public function formatNotification($notification, $text) {
        $sms_message = $text;
        
        $clientName = Yii::$app->db->createCommand('SELECT Name FROM clients WHERE ClientID = ' . $notification['ClientID'])->queryColumn();
        $companyName = Yii::$app->db->createCommand('SELECT CompanyName FROM customers WHERE CustomerID = ' . $notification['CustomerID'])->queryColumn();                
                
        $date = (new \DateTime($notification['AppointmentStartTime']))->format('M j');
        $time = (new \DateTime($notification['AppointmentStartTime']))->format('H:i');
        $sms_message = str_replace(Placeholder_AppointmentTime, 
                                    $date . ' ' . Yii::t('app', 'at'). ' ' . $time, 
                                    $sms_message);
                
        $sms_message = str_replace(Placeholder_Client, 
                                    $clientName[0], 
                                    $sms_message);
        
        $sms_message = str_replace(Placeholder_Company, 
                                    $companyName[0], 
                                    $sms_message);
        
        return $sms_message;
    }

    
    
    protected function recordPaidOrder($id, $details = '')
    {
        $order = Orders::findOne($id);
        if ($order == null) {
            throw new NotFoundHttpException('Order not found, id: ' . $id);
        }
        
        
		$customer = Customers::findOne($order->CustomerID);
		$hasInvoice = false;

		if ($order->OrderStatusID == ORDER_PAID) {
			return;
		}
		
		$connection = \Yii::$app->db;
		$model = $connection->createCommand('SELECT Max(InvoiceID)+1 FROM invoices');
		$invoiceID = $model->queryScalar();
		if (empty($invoiceID)) {
			$invoiceID = 1;
		}
		
		$connection = \Yii::$app->db;		
		$transaction = $connection->beginTransaction();
		try {
			$user_model=$connection->createCommand()
						->insert('invoices', [
								'InvoiceID' => $invoiceID,
								'OrderID' => $id,
								'InvoiceDate' => new \yii\db\Expression('NOW()'),
								'CustomerID' => $order->CustomerID,
								'OrderText' => 'Абонамент номер: '.$id,
								'Amount' => $order->Amount,
								'CompanyName' => $order->CompanyName,
								'Address' => $order->Address,
								'PaymentMethodID' => $order->PaymentMethodID,
								'Notified' => 0,
							])
						->execute();
			
			$connection	->createCommand()
						->update('orders', 
								  [
									'OrderStatusID' => ORDER_PAID,
									'Details' => $details,
									'PayDate' => new \yii\db\Expression('NOW()'),
								  ],
								  'OrderID = ' . $order->OrderID
								)
						->execute();

			// plan extending
			if ($customer->PlanID == $order->PlanID) {
				$connection	->createCommand()
							->update('customers', 
									  [
										'Balance' => $customer->Balance - $order->BalanceMinus,
										'PlanID' => $order->PlanID,
										'PlanExpirationTime' => new \yii\db\Expression('date_add(PlanExpirationTime, interval '.$order->Period.' month)'),
										'PlanActive' => 1,
										'Notified' => 0,
									  ],
									  'PlanExpirationTime > sysdate() AND CustomerID = ' . $order->CustomerID
									)
							->execute();
				$connection	->createCommand()
							->update('customers', 
									  [
										'Balance' => $customer->Balance - $order->BalanceMinus,
										'PlanID' => $order->PlanID,
										'PlanExpirationTime' => new \yii\db\Expression('date_add(sysdate(), interval '.$order->Period.' month)'),
										'PlanActive' => 1,
										'Notified' => 0,
									  ],
									  'PlanExpirationTime <= sysdate() AND CustomerID = ' . $order->CustomerID
									)
							->execute();
			} else {
				// plan upgrade
				// the new plan is effective starting from now
				// the previous plan remaining time goes to the Customers.Balance
				$m = $connection->createCommand('SELECT datediff(PlanExpirationTime, Now())/30 FROM customers WHERE CustomerID = ' . $customer->CustomerID);
				$months = $m->queryScalar();
			
				$current_plan = Plans::findOne($customer->PlanID);
				$balance_new = $months * $current_plan->Price;
								
				$connection	->createCommand()
							->update('customers', 
									  [
										'Balance' => $customer->Balance + $balance_new - $order->BalanceMinus,
										'PlanID' => $order->PlanID,
										'PlanExpirationTime' => new \yii\db\Expression('date_add(sysdate(), interval '.$order->Period.' month)'),
										'PlanActive' => 1,
										'Notified' => 0,
									  ],
									  'CustomerID = ' . $order->CustomerID
									)
							->execute();					
			}
			
			
			$transaction->commit();						
			$hasInvoice = true;
		} catch(Exception $e) {
			$this->error($e->getTraceAsString());
			$transaction->rollback();
		}
		
		return;
    }	

    
    
    public function onContactRequest($appointment) 
    {
        $customer = Customers::findOne(['CustomerID' => $appointment->CustomerID]);    
        if ($customer->NotificationPrefs == Notification_Type_Email ||
            $customer->NotificationPrefs == Notification_Type_Email_And_SMS) 
        {
            $logins = Logins::findAll(['CustomerID' => $appointment->CustomerID]);
            foreach ($logins as $login) {
                \Yii::$app->mail->compose('contactRequestEmail', 
												['user' => $user, 'password' => $user->password])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                    ->setTo($login->Email)
                    ->setSubject(\Yii::$app->name . ' ' . Yii::t('app', 'Registration'))
                    ->send();
            }
        }
    }
    
    
    
    public function getOption($name) {
        return Options::getOption($name);
    }    
    
    
    
    // $client is yiiauth/BaseClient
    function getAttrEmail($client) {
        $attributes = $client->getUserAttributes();
        if ($client->getName() == 'google') {
            $email = $attributes['emails'][0]['value'];
        } else if ($client->getName() == 'facebook') {
            $email = $attributes['email'];
        }        
        return $email;
    }        
}
